import { createStore, applyMiddleware } from 'redux';
//import postsReducer from '../redcuers/posts';
//import filtersReducer from '../redcuers/filters';
import rootReducer from './reducers/index';
import { persistStore, persistReducer } from 'redux-persist';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';

const persistConfig = {
    key: 'root',
    storage: storage,
    stateReconciler: hardSet
};

const pReducer = persistReducer(persistConfig,rootReducer);

export const store = createStore(
    pReducer,
    applyMiddleware(thunk)
);

export const persistor = persistStore(store);