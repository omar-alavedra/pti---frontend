import { 
    
    SIGNIN_FAILED,
    SIGNIN_SUCCESS,
    SIGNUP_FAILED,
    SIGNUP_SUCCESS,
    SAVE_CONTRACTS,
    SAVE_USER_INFO,
    GET_CONTRACTS,
    GET_TRIP,
    LOG_OUT,
} from  './actionTypes';

export const setUserInfo = (userInfo) => {
    // console.log("we made it to the user info action")
    // console.log(userInfo)
    return {
        type: SET_USER_INFO,
        userInfo: userInfo
    }
}


export const signupFailed = (code,error) => {
    return {
        type: SIGNUP_FAILED,
        errorCode: code,
        errorMessage: error,
    }
}

export const signupSuccess = (userName,token,phone) => {
    return {
        type: SIGNUP_SUCCESS,
        token: token,
        userName: userName,
        phone: phone
        
    }
}

export const signinFailed = (code,error) => {
    //console.log("arriba a la action");
    return {
        type: SIGNIN_FAILED,
        errorCode: code,
        errorMessage: error,
    }
}

export const signinSuccess = (token,answer) => {
    //console.log(answer);
    return {
        type: SIGNIN_SUCCESS,
        token: token,
        answer: answer,
        verified: false
    }
}
export const saveContracts = (contracts) => {
    //console.log(answer);
    return {
        type: SAVE_CONTRACTS,
        contracts: contracts,
    }
}

export const saveUserInfo = (userInfo) => {
    //console.log(answer);
    return {
        type: SAVE_USER_INFO,
        userInfo: userInfo,
    }
}
export const logOut = () => {
    return {
        type: LOG_OUT
    }
}



export const getContracts = (contracts) =>{
    //console.log('we made it to the action');
    //console.log(trips.catalog);
    return {
        type: GET_CONTRACTS,
        contracts: contracts.catalog,
        // images: trips.images
    }
}

export const getTrip = (trip) =>{
    // console.log('we made it to the action');
    // console.log(trip);
    return {
        type: GET_TRIP,
        trip: trip.trip,
        images: trip.images
    }
}
