//      AUTH TYPES


export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';

export const SIGNUP_FAILED = 'SIGNUP_FAILED';


export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';

export const SIGNIN_FAILED = 'SIGNIN_FAILED';

export const LOG_OUT = 'LOG_OUT';


//      GET INFO FROM SERVER

export const GET_CONTRACTS = 'GET_CONTRACTS';
export const SAVE_CONTRACTS = 'SAVE_CONTRACTS';
export const SAVE_USER_INFO = 'SAVE_USER_INFO';
export const GET_TRIP = 'GET_TRIP';

// ----------------------------------------------------------
