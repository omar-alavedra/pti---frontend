import { 
    signupFailed, 
    signupSuccess, 
    signinSuccess,
    setUserInfo, 
    saveUserInfo,
    signinFailed, 
    sendFirebseToken ,
    saveContracts
} from "./actions";
const axios = require('axios');


export const authSignup = (authData,fatherID) => {
    console.log(authData)
    return dispatch =>  {
        //dispatch(trySignup())
        // console.log(authData)
        let data = JSON.stringify({
            email: authData.email,
            password: authData.password,
            name: authData.name,
            lastName: authData.lastName,
            age: 18,
            userType: "Consumer",
            walletId:"testIDWallet"
        })
        axios.post('https://cors-anywhere.herokuapp.com/http://34.70.95.68:3000/api/auth/signup', data, {headers: {'Content-Type': 'application/json'}} )
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
        
}
}
export const getInfoUser = (userId, token) => {
    return dispatch => {
        // console.log(userId)
        // //GET USER INFO
        fetch('http://34.70.95.68:3000/api/auth/signin' + userId,{
                    method: 'GET',
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        'Authorization': 'Bearer ' + token
                    }
                })
                .then(res => res.json())
                .then(parsedRes => {
                    console.log(parsedRes)
                    if (parsedRes.success) {
                        //Aqui tractes l'objecte de user com vulguis
                        // console.log(parsedRes.success)
                        dispatch(setUserInfo(parsedRes))
                    }
                })
                .catch(err =>{
                    console.log("Error getting user info form the server: ", err)
                })
    }
}
export const authGetUserInfo = (authData) =>{
    console.log(authData)
    return dispatch => {
        
        axios.post('https://cors-anywhere.herokuapp.com/http://34.70.95.68:3000/api/auth/profile',{'auth-token':authData,},{headers: {'auth-token':authData, 'Content-Type': 'application/json', "Accept": "application/json", } } )
            .then(function (response) {
              console.log(response);
              // this.props.getContracts(response)
              dispatch(saveUserInfo(response.data));
  
            })
            .catch(function (error) {
              console.log(error);
            });
        }
    }
export const authGetContracts = (authData) =>{
        console.log(authData)
        return dispatch => {
            axios.post('https://cors-anywhere.herokuapp.com/http://34.70.95.68:3000/api/contract/listContract',{'auth-token':authData,},{headers: {'auth-token':authData, 'Content-Type': 'application/json', "Accept": "application/json", } } )
            .then(function (response) {
              console.log(response);
              // this.props.getContracts(response)
              dispatch(saveContracts(response.data));
  
            })
            .catch(function (error) {
              console.log(error);
            });
            }
        }
export const authSignin = (authData) =>{
    console.log(authData)
    return dispatch => {
        
        if(!authData.email.valid) dispatch(signupFailed(500,"invalid email"));
        else if (!authData.password.valid) dispatch(signupFailed(500,"invalid password"));
        let data = JSON.stringify({
            email: authData.email.value,
            password: authData.password.value,
        })
        let token=''
        let name=''
        axios.post('https://cors-anywhere.herokuapp.com/http://34.70.95.68:3000/api/auth/signin', data, {headers: {'Content-Type': 'application/json', "Accept": "application/json"}} )
          .then(function (response) {
            name="auth-token"
            token=response.headers[name]
            // console.log(token);
            
            dispatch(signinSuccess(response.headers["auth-token"], response));
            
          })
          .catch(function (error) {
            console.log(error);
          });
          console.log(token)
         
        
        
    }
}

