import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import TripReducer from './TripReducer';

const rootReducer = combineReducers({
    authReducer: AuthReducer,
    tripReducer: TripReducer
});

export default rootReducer;