import { 
    GET_CONTRACTS,
    GET_TRIP
} from '../actions/actionTypes'

const initialState = {
    data: {
        trips: [],
        trip: undefined,
        loaded: false,
        tripLoaded:false,
        tripImages: []
    }
    
}

const TripReducer = (state = initialState, action) => {
    // console.log(action)
    // console.log(state);
    switch (action.type) {
        case GET_CONTRACTS:
            return {
                ...state,
                data: {
                    ...state.data,
                    contracts: action.contracts,
                    loaded: true,
                    // tripImages: action.images
                }
            }
        case GET_TRIP:
            return {
                ...state,
                data: {
                    ...state.data,
                    trip: action.trip,
                    tripLoaded: true,
                    tripImages: action.images
                }
            }
        default:
            return state;
    }
};

export default TripReducer;