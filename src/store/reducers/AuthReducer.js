import { 
    SIGNIN_FAILED,
    SIGNIN_SUCCESS,
    SIGNUP_FAILED,
    SIGNUP_SUCCESS,
    LOG_OUT,
    FACE_FAILED,
    FACE_SUCCESS,
    SEND_FIREBASE_TOKEN,
    FATHER_ID,
    VERIFY_SUCCESS,
    VERIFY_FAILED,
    SET_USER_INFO,
    LIKE_UNLIKE_TRIP,
    SAVE_CONTRACTS,
    SAVE_USER_INFO
} from '../actions/actionTypes'

const initialState = {
    facebookLogged: false,
    facebookState: {
        status: '',
        authResponse: {
            accessToken:'',
            expiresIn: '',
            signedRequest:'',
            userID: ''
        }
    },
    serverState: {
        logged: false,
        USER_ID: '',
        TOKEN_LOGIN: '',
        phone:'',
        answer: '',
        userInfo: '',
        contracts:[],
        userInfo:''
    },
    errorMessage: '',
    errorCode: '',
    firebaseToken: '',
    verifyError: false
}

const AuthReducer = (state = initialState, action) => {
    // console.log(action)
    switch (action.type) {
        case SIGNIN_FAILED:
            return {
                ...state,
                serverState: {
                logged: false,
                errorCode: action.errorCode,
                errorMessage: action.errorMessage,
                }
            }
        case LOG_OUT:
            return {
                ...state,
                serverState: {
                    ...state.serverState,
                    logged: false,
                    USER_ID: '',
                    TOKEN_LOGIN: '',
                    phone:'',
                    answer: '',
                    userInfo: '',
                },
            }
        case SIGNIN_SUCCESS:
            return {
                ...state,
                serverState: {
                    ...state.serverState,
                    isFetching: false,
                    logged: true,
                    TOKEN_LOGIN: action.token,
                    answer: action.answer
                },
                verifyState: {
                    ...state.verifyState,
                    phone: action.phone,
                    verified: action.verified
                }
            }
        case SIGNUP_FAILED:
            return {
                ...state,
                serverState: {
                    logged: false,
                    errorCode: action.errorCode,
                    errorMessage: action.errorMessage,
                }
            }
        case SIGNUP_SUCCESS:
            return {
                ...state,
                serverState: {
                    ...state.serverState,
                    logged: true,
                    TOKEN_LOGIN: action.token,
                    USER_ID: action.userName,
                    phone: action.phone
                }
            }
        case SAVE_CONTRACTS:
            return {
                ...state,
                serverState: {
                    ...state.serverState,
                    contracts:action.contracts
                }
            }
            case SAVE_USER_INFO:
            return {
                ...state,
                serverState: {
                    ...state.serverState,
                    userInfo:action.userInfo
                }
            }
        default:
            return state;
    }
};

export default AuthReducer;