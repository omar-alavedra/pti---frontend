import React from 'react';
import { NavLink } from 'react-router-dom';
import { Link } from "react-router-dom";

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return(
            <div className="footer">
            <nav className="footer__navigation">
                <Link to="/" className="footer__title">
                    {/* <IconBitch/> */}
                    {/* <img src={require('./images/logo/tuprobadorBlanco.png')}/> */}
                    <p>EnergySource</p>
                </Link>


                <div className="footer__content">
                    {/* <div className="footer__content__item">
        
                    <a>Empezar</a>
                        <ul>
                            <li><Link to="/joinUs">Regístrate</Link> </li>
                            <li> <a>Descárgate la app iOS</a> </li>
                            <li> <a>Descárgate la app Android</a> </li>
                        </ul>
                    </div> */}

                    <div className="footer__content__item">
                        <a>Conócenos</a>
                        <ul>
                            <li><Link to="https://www.instagram.com/energySource/" target="_blank">¿Qué es Tuprobador?</Link></li>
                            {/* <li><Link to="/howto" target="_blank">¿Cómo ganar el surftrip a Maldivas?</Link></li>
                            <li><Link to="https://www.sharryup.com/community-surf-trips/" target="_blank">Nuestros community surf trips</Link></li> */}
                        </ul>
                    </div> 

                    <div className="footer__content__item">
                        <a>Legal</a>
                        <ul>
                            <li> <a href={"mailto:tuprobador@gmail.com"}>Contacto </a></li>
                            {/* <li> <Link to="/legal" target="_blank"> Aviso Legal </Link></li>
                            <li> <Link to="/terms" target="_blank">Términos y condiciones</Link> </li> */}
                            <li> <Link to="/politica-de-privacidad-tuprobador" target="_blank">Política de Privacidad</Link> </li>
                        </ul>
                    </div>

                    <div className="footer__content__item">
                        <a>Síguenos</a>
                        <div className="footer__media">
                            <Link to="https://www.instagram.com/energySource/" target="_blank"> <img src={require("./images/footer/instagram_icon.png")}/> </Link>
                            <Link to="https://www.facebook.com/energySource-403055473672917/" target="_blank"> <img src={require("./images/footer/facebook-logo@3x.png")}/> </Link>
                            <Link to="https://www.youtube.com/watch?v=energySource" target="_blank"> <img className="youtube__icon" src={require("./images/footer/youtube-play-button@3x.png")}/> </Link>
                        </div>
                    </div>

                </div>
                <div className="footer__bottom">
                    <div className="footer__spacer"/>
                    <div className="copyright">
                        <a>© 2019 EnergySource</a>
                    </div>
                </div>
            </nav>
        </div>
        );
    }
};
