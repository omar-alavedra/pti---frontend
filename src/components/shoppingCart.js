import React from 'react';
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom';
import { isBrowser, isMobileOnly} from "react-device-detect";

import {deleteFromCart } from '../store/actions/index';

class ShoppingCart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayMenu: false,
            redirectCheckout: false
        }
        this.checkout = this.checkout.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    };
    checkout = () => {
        this.setState({
            ...this.state,
            redirectCheckout: true
        })
    }
    deleteItem = (id) => {
        this.props.deleteFromCart(id)
    }
    
    render() {
        let itemList = this.props.items.map((item,count)=>{
            return(
                <div className="itemCard" key={count}>
                        <div>
                            {item.img}
                        </div>
                        <div className="card-image">
                            <span className="card-title">{item.title}</span>
                        </div>
                        <button onClick={()=>this.deleteItem(item.id)}>delete</button>
                        <div className="card-content">
                            <p>{item.desc}</p>
                            <p><b>Price: {item.price}$</b></p>
                        </div>
                 </div>
            )
        })
        if (this.state.redirectCheckout) {
            return <Redirect to='/checkout'/>;
        }

        if(isBrowser){
            return(
                <div className="shoppingCart">
                    <div className="contentWrapper">
                        <h3>Your Cart</h3>
                        {itemList}
                    </div>    
                    <div className="buttonContainer">
                        <button onClick={()=>this.checkout()}>CHECKOUT</button>
                    </div>                
                </div>                
            )
        }
        if(isMobileOnly){

        }
        
    }
};


const mapStateToProps = (state)=>{
    // console.log(state)
    return {
        items: state.cartReducer.addedItems
    }
}

const mapDispatchToProps= (dispatch)=>{
    return{
        deleteFromCart: (id)=>{dispatch(deleteFromCart(id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ShoppingCart);