import React from 'react';
import { Link } from "react-router-dom";

export default class FooterMobile extends React.Component {
    render() {
        return(
            <div className="footer__mobile">
                <nav className="footer__navigation__mobile">
                    <p className="footer__title__mobile">
                        {/* <img src={require('./images/logo/tuprobadorBlanco.png')}/> */}
                        EnergySource
                    </p>
                    <div className="footer__content__mobile">
                            <div className="footer__content__mobile1">
                                <div className="footer__content__item__mobile">
                                    <a>Conócenos</a>
                                    <ul>
                                        <li><Link to="https://www.instagram.com/energySource/" target="_blank">¿Qué es EnergySource?</Link></li>
                                        {/* <li><Link to="/howto">¿Cómo ganar el surftrip a Maldivas?</Link></li>
                                        <li><Link to="https://www.youtube.com/channel/UCpYAv5tBKFSbNZ-m0_zvPaw" target="_blank">Nuestros community surf trips</Link></li> */}
                                    </ul>
                                </div>
                                <div className="footer__content__item__mobile">
                                    <a>Síguenos</a>
                                    <div className="footer__media__mobile">
                                        <Link to="https://www.instagram.com/energySource/" target="_blank"> <img id="img0" src={require("./images/footer/instagram_icon.png")}/> </Link>
                                        <Link to="https://www.facebook.com/energySource-energySource/" target="_blank"> <img id="img1" src={require("./images/footer/facebook-logo@3x.png")}/> </Link>
                                        <Link to="https://www.youtube.com/watch?v=energySource" target="_blank"> <img id="img2" src={require("./images/footer/youtube-play-button@3x.png")}/> </Link>
                                    </div>
                                </div>
                                {/* <div className="footer__content__item__mobile">
                                    <a>Empezar</a>
                                    <ul>
                                        <li><Link to="/joinUs">Regístrate</Link></li>
                                        <li><a>Descárgate la app iOS</a></li>
                                        <li><a>Descárgate la app Android</a></li>
                                    </ul>
                                </div> */}
                                
                            </div>
                            <div className="footer__content__mobile2">
                                <div className="footer__content__item__mobile">
                                <a>Legal</a>
                                <ul>
                                    <li><a href={"mailto:energysource@gmail.com"} >Contacto</a></li>
                                    {/* <li><Link to="/legal">Aviso Legal</Link></li> */}
                                    {/* <li><Link to="/terms">Términos y condiciones</Link> </li> */}
                                    <li><Link /*to="/politica-de-privacidad-tuprobador"*/ > Política de Privacidad</Link></li>
                                </ul>
                                </div>
                                
                            </div>
                    </div>
                    <div className="footer__bottom__mobile">
                        <div className="footer__spacer__mobile"/>
                        <div className="copyright__mobile">
                            <a>© 2019 EnergySource</a>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
};
