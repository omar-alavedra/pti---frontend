import React from "react";
import { isBrowser, isMobileOnly} from "react-device-detect";

const CustomForm = ({ status, message, onValidated }) => {
    let email;
    let name;
    const submit = (e) => {
      // console.log(name.value)
      // email &&
      // email.value.indexOf("@") > -1 &&
      // onValidated({
      //   FNAME: name.value,
      //   EMAIL: email.value,
      // });
      // e.preventDefault();
  
    }
      if(isBrowser){
        return(
          <div className="componentMailchimp">
          <form onSubmit={submit} className="subscribeInput">
            <div className="inputGroup">
            <input
                ref={node => (name = node)}
                type="text"
                placeholder="John Gabana"
                // className="subscribeInput"
              //   value={status === "success" ? "" : null}
              />
              {/* <input
                ref={node => (birthday = node)}
                type="text"
                placeholder="birthday"
                // className="subscribeInput"
              //   value={status === "success" ? "" : null}
              /> */}
              <input
                ref={node => (email = node)}
                type="email"
                placeholder="info@energySource.com"
                // className="subscribeInput"
              //   value={status === "success" ? "" : null}
              />
              {/* <div className="input-group-append"> */}
                <button
                  type="submit"
                  className="btn btn-secondary"
                >
                  UNIRSE
                </button>
              {/* </div> */}
            </div>
          </form>
          <div className="messageAlert">
            {status === "sending"?<div className="loading"> Cargando...</div>:null}
            {status === "error"?(
              <div
                className="danger"
                // role="alert"
                // dangerouslySetInnerHTML={{ __html: message }}
              >Dirección de correo inválida o ya suscrita.</div>
            ):null}
            {status === "success"?(
              <div
                className="sucess"
                // role="alert"
                // dangerouslySetInnerHTML={{ __html: message }}
              >Bienvenido! Estate atento a tu correo!</div>

            ):null}
          </div>
        </div>
        )
      }
      else if(isMobileOnly){
        return(
          <div className="componentMailchimpMobile">
            <form onSubmit={submit} className="subscribeInputMobile">
              
              <div className="inputGroupMobile">
              <input
                  ref={node => (name = node)}
                  type="text"
                  placeholder="John Gabana"
                  // className="subscribeInput"
                //   value={status === "success" ? "" : null}
                />
                <input
                  ref={node => (email = node)}
                  type="email"
                  placeholder="info@energySource.com"
                  // className="subscribeInput"
                //   value={status === "success" ? "" : null}
                />
                {/* <div className="input-group-append"> */}
                  <button
                    type="submit"
                    className="btn btn-secondary"
                  >
                    UNIRSE
                  </button>
                  
                {/* </div> */}
              </div>
            </form>
            <div className="messageAlertMobile">
              {status === "sending"?<div className="loadingMobile"> Cargando...</div>:null}
              {status === "error"?(
                <div
                  className="dangerMobile"
                  // role="alert"
                  // dangerouslySetInnerHTML={{ __html: message }}
                >Dirección de correo inválida o ya suscrita.</div>
              ):null}
              {status === "success"?(
                <div
                  className="sucessMobile"
                  // role="alert"
                  // dangerouslySetInnerHTML={{ __html: message }}
                >Bienvenido! Estate atento a tu correo!</div>
  
              ):null}
            </div>
          </div>
        )
      }
  };

export default CustomForm;