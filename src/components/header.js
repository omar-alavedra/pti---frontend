import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

//-- REDUX
import { logOut } from '../store/actions/index';

//-- COMPONENTS
// import ShoppingCart from '../components/shoppingCart';
// import HeaderMenu from './headerMenu';

//--3rd party components
import HamburgerMenu from 'react-hamburger-menu';


class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayMenu: false,
            displayCart: false
        }    
        this.showMenu = this.showMenu.bind(this);
        this.chooseGender = this.chooseGender.bind(this);
    };
    showMenu = () => {
        this.setState({
            displayMenu: !this.state.displayMenu
        })
    }

    showChart = () => {
        this.setState({
            displayCart: !this.state.displayCart
        })
    }
    chooseGender = (gender) => {
        console.log(gender)
        this.props.selectGender(gender)
    }
    componentDidMount(){
        console.log(this.props.logged)
    }
    logout(){
        this.props.logOut()
    }
    render() {
        if(!this.props.logged){
            return(
                <div>
                    <header className={"headerScroll"} style={{ boxShadow: 'no'}}>
                        <div className="menu">
                            {this.displayMenu}
                        </div>
                        <nav className="header__navigation">
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                                <div className="link1" onClick={()=>this.chooseGender("FEMALE")}>
                                                    <Link to="/">EnergySource</Link>
                                                </div>
                                                <div className="link__separator">
                                                    |
                                                </div>
                                            </nav>
                                </div>
                                <div className="header__spacer"></div>
                                <div className="logo__wrapper">
                                </div>
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                                <div className="link1">
                                                    <Link to="/signUp">Signup</Link>
                                                </div>
                                                <div className="link__separator">
                                                    |
                                                </div>
                                                <div className="link3">
                                                    <Link to="/login">Login</Link>
                                                </div>
                                                   
                                            </nav>
                                </div>
    
                        </nav>
                    </header>
                </div>
            );
        }
        else{
            return(
                <div>
                    {this.state.displayCart?<ShoppingCart/>:null}
                    <header className={"headerScroll"} style={{ boxShadow: 'no'}}>
                        <div className="menu">
                            {this.displayMenu}
                        </div>
                        <nav className="header__navigation">
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                                <div className="link1" onClick={()=>this.chooseGender("FEMALE")}>
                                                    <Link to="/">EnergySource</Link>
                                                </div>
                                                <div className="link__separator">
                                                    |
                                                </div>
                                            </nav>
                                </div>
                                <div className="header__spacer"></div>
                               
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                               
                                                <div className="link3" onClick={()=>this.logout()}>
                                                    <Link to="/login">Logout</Link>
                                                </div>
                                            </nav>
                                </div>
    
                        </nav>
                    </header>
                </div>
            );
        }

    }
};

const mapStateToProps = (state)=>{
    // console.log(state)
    return {
        // gender: state.storeReducer.gender
    }
}

const mapDispatchToProps= (dispatch)=>{
    return {
        logOut: () => dispatch(logOut()),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);