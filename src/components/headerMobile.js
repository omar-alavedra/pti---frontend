import React from 'react';
// import { connect } from 'react-redux';
// import SignModal from '../components/signModal';

import { Link } from 'react-router-dom';
import HamburgerMenu from 'react-hamburger-menu';
// import HeaderMenuMobile from './headerMenuMobile';

class HeaderMobile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayMenu: false,
        }    
        this.showMenu = this.showMenu.bind(this);
    };
   
    showMenu = () => {
        this.setState({
            displayMenu: !this.state.displayMenu
        })

    }
    render() {
        if(!this.props.logged){
            return(
                <div>
                    <header className={"headerScroll"} style={{ boxShadow: 'no'}}>
                        <nav className="headerMobile__navigation">
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                                <div className="link1" onClick={()=>this.chooseGender("FEMALE")}>
                                                    <Link to="/">EnergySource</Link>
                                                </div>
                                                <div className="link__separator">
                                                    |
                                                </div>
                                            </nav>
                                </div>
                                <div className="header__spacer"></div>
                                <div className="logo__wrapper">
                                </div>
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                                <div className="link1">
                                                    <Link to="/signUp">Signup</Link>
                                                </div>
                                                <div className="link__separator">
                                                    |
                                                </div>
                                                <div className="link3">
                                                    <Link to="/login">Login</Link>
                                                </div>
                                                   
                                            </nav>
                                </div>
    
                        </nav>
                    </header>
                </div>
            );
        }
        else{
            return(
                <div>
                    {this.state.displayCart?<ShoppingCart/>:null}
                    <header className={"headerScroll"} style={{ boxShadow: 'no'}}>
                        <div className="menu">
                            {this.displayMenu}
                        </div>
                        <nav className="headerMobile__navigation">
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                                <div className="link1" onClick={()=>this.chooseGender("FEMALE")}>
                                                    <Link to="/">EnergySource</Link>
                                                </div>
                                                <div className="link__separator">
                                                    |
                                                </div>
                                            </nav>
                                </div>
                                <div className="header__spacer"></div>
                               
                                <div className="header__navlinks">
                                            <nav className="links__wrapper">
                                               
                                                <div className="link3">
                                                    <Link to="/login">Logout</Link>
                                                </div>
                                            </nav>
                                </div>
    
                        </nav>
                    </header>
                </div>
            );
        }

    }
};

export default HeaderMobile;