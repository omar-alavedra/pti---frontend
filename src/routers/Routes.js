import React from 'react';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom';
// import NotFoundPage from '../components/notFound';
import Home from '../pages/home';
import User from '../pages/user';
import Contract from '../pages/contract';
import SignUp from '../pages/signUp';
import Login from '../pages/login';
import NotFound from '../pages/notFound';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';

const Routes = (match) => {
    return(
        <Router>
            <div>

                <Provider store={store}>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/user" component={User} />
                        <Route exact path="/contract" component={Contract} />
                        <Route exact path="/signUp" component={SignUp} />
                        <Route exact path="/login" component={Login} />
                       
                        <Route component={NotFound} />
                    </Switch>
                </Provider>
            </div>
        </Router>
    )
}

export default Routes;
