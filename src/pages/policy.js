import React from 'react';
import { Redirect } from 'react-router';
import { Link } from "react-router-dom";

import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';

import { isBrowser, isMobileOnly} from "react-device-detect";

export default class Policy extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // redirect: false,
        };
    }
    
    render() {
        if(isMobileOnly) {
            return(
                <div className="legal__mobile" >
                <div className="legal__wrapper__mobile">
                    <div className="header_top_mobile">
                    <Link to="/">
                        <img className="logo__top__mobile" src={require('../components/images/logo/tuprobadorBlanco.png')}/>
                    </Link>
                    </div>
                    
                    <div className="spacer"/>
                    <div className="legal__content__mobile" >
                        <h2 id="title">Política de privacidad </h2>
                        <div className="paragraph__mobile">
                            <h3>TÉRMINOS LEGALES</h3>
                            <p>ENERGYSOURCE te informa sobre su Política de Privacidad respecto del tratamiento y protección de los datos de carácter personal de los usuarios y clientes que puedan ser recabados por la navegación o contratación de servicios a través del sitio Web tuprobadorapp.com.
                            En este sentido, el Titular garantiza el cumplimiento de la normativa vigente en materia de protección de datos personales, reflejada en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y de Garantía de Derechos Digitales (LOPD GDD). Cumple también con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas (RGPD).
                            El uso de sitio Web implica la aceptación de esta Política de Privacidad así como las condiciones incluidas en el Aviso Legal.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Principios aplicados en el tratamiento de datos</h3>
                            <p>En el tratamiento de tus datos personales, el Titular aplicará los siguientes principios que se ajustan a las exigencias del nuevo reglamento europeo de protección de datos:
                                {/* <br/>Algunos Servicios le permiten proporcionarnos información directamente. Por ejemplo: */}
                                <div className="sub-paragraph__mobile">
                                    <br/>· Principio de licitud, lealtad y transparencia: El Titular siempre requerirá el consentimiento para el tratamiento de tus datos personales que puede ser para uno o varios fines específicos sobre los que te informará previamente con absoluta transparencia.
                                    <br/>· Principio de minimización de datos: El Titular te solicitará solo los datos estrictamente necesarios para el fin o los fines que los solicita.   
                                    <br/>· Principio de limitación del plazo de conservación: Los datos se mantendrán durante el tiempo estrictamente necesario para el fin o los fines del tratamiento.
                                    <br/>· El Titular te informará del plazo de conservación correspondiente según la finalidad. En el caso de suscripciones, el Titular revisará periódicamente las listas y eliminará aquellos registros inactivos durante un tiempo considerable.  
                                    <br/>· Principio de integridad y confidencialidad: Tus datos serán tratados de tal manera que su seguridad, confidencialidad e integridad esté garantizada.
                                    <br/>· Debes saber que el Titular toma las precauciones necesarias para evitar el acceso no autorizado o uso indebido de los datos de sus usuarios por parte de terceros.
                                </div>                             
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Obtención de datos personales</h3>
                            <p>Para navegar por tuprobadorapp.com no es necesario que facilites ningún dato personal. <br/>
                                Los casos en los que sí proporcionas tus datos personales son los siguientes:
                                <div className="sub-paragraph__mobile">
                                    <br/>· Al contactar a través de los formularios de contacto o enviar un correo electrónico.
                                    <br/>· Al realizar un comentario en un artículo o página.
                                    <br/>· Al inscribirte en un formulario de suscripción o un boletín que el Titular gestiona con MailChimp.
                                </div>           
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Tus derechos</h3>
                            <p>Podemos divulgar tu información personal si se nos requiere por ley o si violas nuestros Términos de Servicio.</p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Tus derechos</h3>
                            <p>El Titular te informa que sobre tus datos personales tienes derecho a:
                                <div className="sub-paragraph__mobile">
                                    <br/>· Solicitar el acceso a los datos almacenados.
                                    <br/>· Solicitar una rectificación o la cancelación.
                                    <br/>· Solicitar la limitación de su tratamiento.
                                    <br/>· Oponerte al tratamiento.
                                    <br/>· Solicitar la portabilidad de tus datos.
                                </div>         
                                <br/>El ejercicio de estos derechos es personal y por tanto debe ser ejercido directamente por el interesado, solicitándolo directamente al Titular, lo que significa que cualquier cliente, suscriptor o colaborador que haya facilitado sus datos en algún momento puede dirigirse al Titular y pedir información sobre los datos que tiene almacenados y cómo los ha obtenido, solicitar la rectificación de los mismos, solicitar la portabilidad de sus datos personales, oponerse al tratamiento, limitar su uso o solicitar la cancelación de esos datos en los ficheros del Titular.
                                <br/>Para ejercitar tus derechos de acceso, rectificación, cancelación, portabilidad y oposición tienes que enviar un correo electrónico a tuprobador@gmail.com junto con la prueba válida en derecho como una fotocopia del D.N.I. o equivalente.
                                <br/>Tienes derecho a la tutela judicial efectiva y a presentar una reclamación ante la autoridad de control, en este caso, la Agencia Española de Protección de Datos, si consideras que el tratamiento de datos personales que te conciernen infringe el Reglamento.
                                <br/>Finalidad del tratamiento de datos personales
                                <br/>Cuando te conectas al sitio Web para mandar un correo al Titular, te suscribes a su boletín o realizas una contratación, estás facilitando información de carácter personal de la que el responsable es el Titular. Esta información puede incluir datos de carácter personal como pueden ser tu dirección IP, nombre y apellidos, dirección física, dirección de correo electrónico, número de teléfono, y otra información. Al facilitar esta información, das tu consentimiento para que tu información sea recopilada, utilizada, gestionada y almacenada por superadmin.es , sólo como se describe en el Aviso Legal y en la presente Política de Privacidad.
                                <br/>Los datos personales y la finalidad del tratamiento por parte del Titular es diferente según el sistema de captura de información:
                                <div className="sub-paragraph__mobile">
                                    <br/>· Formularios de contacto: El Titular solicita datos personales entre los que pueden estar: Nombre y apellidos, dirección de correo electrónico, número de teléfono y dirección de tu sitio Web con la finalidad de responder a tus consultas.
                                    <br/>· Por ejemplo, el Titular utiliza esos datos para dar respuesta a tus mensajes, dudas, quejas, comentarios o inquietudes que puedas tener relativas a la información incluida en el sitio Web, los servicios que se prestan a través del sitio Web, el tratamiento de tus datos personales, cuestiones referentes a los textos legales incluidos en el sitio Web, así como cualquier otra consulta que puedas tener y que no esté sujeta a las condiciones del sitio Web o de la contratación.
                                    <br/>· Formularios de suscripción a contenidos: El Titular solicita los siguientes datos personales: Nombre y apellidos, dirección de correo electrónico, número de teléfono y dirección de tu sitio web para gestionar la lista de suscripciones, enviar boletines, promociones y ofertas especiales.
                                    <br/>· Los datos que facilites al Titular estarán ubicados en los servidores de The Rocket Science Group LLC d/b/a, con domicilio en EEUU. (Mailchimp).
                                </div>    
                                <br/>Existen otras finalidades por las que el Titular trata tus datos personales:
                                <div className="sub-paragraph__mobile">
                                    <br/>· Para garantizar el cumplimiento de las condiciones recogidas en el Aviso Legal y en la ley aplicable. Esto puede incluir el desarrollo de herramientas y algoritmos que ayuden a este sitio Web a garantizar la confidencialidad de los datos personales que recoge.
                                    <br/>· Para apoyar y mejorar los servicios que ofrece este sitio Web.
                                    <br/>· Para analizar la navegación. El Titular recoge otros datos no identificativos que se obtienen mediante el uso de cookies que se descargan en tu ordenador cuando navegas por el sitio Web cuyas caracterísiticas y finalidad están detalladas en la Política de Cookies .
                                    <br/>· Para gestionar las redes sociales. el Titular tiene presencia en redes sociales. Si te haces seguidor en las redes sociales del Titular el tratamiento de los datos personales se regirá por este apartado, así como por aquellas condiciones de uso, políticas de privacidad y normativas de acceso que pertenezcan a la red social que proceda en cada caso y que has aceptado previamente.
                                </div>    
                                <br/>Puedes consultar las políticas de privacidad de las principales redes sociales en estos enlaces:
                                <div className="sub-paragraph__mobile">
                                    <br/>· Facebook
                                    <br/>· Youtube
                                    <br/>· Instagram
                                </div>   
                                <br/>El Titular tratará tus datos personales con la finalidad de administrar correctamente su presencia en la red social, informarte de sus actividades, productos o servicios, así como para cualquier otra finalidad que las normativas de las redes sociales permitan. En ningún caso el Titular utilizará los perfiles de seguidores en redes sociales para enviar publicidad de manera individual.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Seguridad de los datos personales</h3>
                            <p>Para proteger tus datos personales, el Titular toma todas las precauciones razonables y sigue las mejores prácticas de la industria para evitar su pérdida, mal uso, acceso indebido, divulgación, alteración o destrucción de los mismos.
                            <br/>El sitio Web está alojado en AWS. La seguridad de tus datos está garantizada, ya que toman todas las medidas de seguridad necesarias para ello. Puedes consultar su política de privacidad para tener más información.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Contenido de otros sitios web</h3>
                            <p>Las páginas de este sitio Web pueden incluir contenido incrustado (por ejemplo, vídeos, imágenes, artículos, etc.). El contenido incrustado de otras web se comporta exactamente de la misma manera que si hubieras visitado la otra web.
                            <br/>Si nuestra tienda es adquirida o fusionada con otra empresa, tu información puede ser transferida a los nuevos propietarios, para que podamos seguir vendiéndote productos.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Política de Cookies</h3>
                            <p>Para que este sitio Web funcione correctamente necesita utilizar cookies, que es una información que se almacena en tu navegador web.
                            <br/>En la página Política de Cookies puedes consultar toda la información relativa a la política de recogida, la finalidad y el tratamiento de las cookies.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Legitimación para el tratamiento de datos</h3>
                            <p>La base legal para el tratamiento de tus datos es: el consentimiento.
                            <br/>Para contactar con el Titular, suscribirte a un boletín o realizar comentarios en este sitio Web tienes que aceptar la presente Política de Privacidad.</p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Categorías de datos personales</h3>
                            <p>Las categorías de datos personales que trata el Titular son:
                            <div className="sub-paragraph__mobile">
                                    <br/>· Datos identificativos.
                                </div>   
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Conservación de datos personales</h3>
                            <p>Los datos personales que proporciones al Titular se conservarán hasta que solicites su supresión.</p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Destinatarios de datos personales</h3>
                            <div className="sub-paragraph__mobile">
                                    <p>
                                    <br/>· Mailchimp The Rocket Science Group LLC d/b/a , con domicilio en EEUU. Encontrarás más información en: https://mailchimp.com. The Rocket Science Group LLC d/b/a trata los datos con la finalidad de prestar sus servicios de email el Titulareting al Titular.
                                    <br/>· Google Analytics es un servicio de analítica web prestado por Google, Inc., una compañía de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos (“Google”). Encontrarás más información en: https://analytics.google.com. Google Analytics utiliza “cookies”, que son archivos de texto ubicados en tu ordenador, para ayudar al Titular a analizar el uso que hacen los usuarios del sitio Web. La información que genera la cookie acerca del uso del sitio Web (incluyendo tu dirección IP) será directamente transmitida y archivada por Google en los servidores de Estados Unidos. También puedes ver una lista de los tipos de cookies que utiliza Google y sus colaboradores y toda la información relativa al uso que hacen de cookies publicitarias.
                            </p>
                            </div>   
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Navegación Web</h3>
                            <p>Al navegar por tuprobadorapp.com se pueden recoger datos no identificativos, que pueden incluir, la dirección IP, geolocalización, un registro de cómo se utilizan los servicios y sitios, hábitos de navegación y otros datos que no pueden ser utilizados para identificarte.
                            <br/>El sitio Web utiliza los siguientes servicios de análisis de terceros:
                            <div className="sub-paragraph__mobile">
                                    <br/>· Google Analytics
                            </div> 
                            <br/>El Titular utiliza la información obtenida para obtener datos estadísticos, analizar tendencias, administrar el sitio, estudiar patrones de navegación y para recopilar información demográfica.  
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Exactitud y veracidad de los datos personales</h3>
                            <p>Te comprometes a que los datos facilitados al Titular sean correctos, completos, exactos y vigentes, así como a mantenerlos debidamente actualizados.
                            <br/>Como Usuario del sitio Web eres el único responsable de la veracidad y corrección de los datos que remitas al sitio exonerando a el Titular de cualquier responsabilidad al respecto.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Aceptación y consentimiento</h3>
                            <p>Te comprometes a que los datos facilitados al Titular sean correctos, completos, exactos y vigentes, así como a mantenerlos debidamente actualizados.
                            <br/>Como Usuario del sitio Web eres el único responsable de la veracidad y corrección de los datos que remitas al sitio exonerando a el Titular de cualquier responsabilidad al respecto.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Revocabilidad</h3>
                            <p>Para ejercitar tus derechos de acceso, rectificación, cancelación, portabilidad y oposición tienes que enviar un correo electrónico a tuprobado@gmail.com junto con la prueba válida en derecho como una fotocopia del D.N.I. o equivalente.
                            <br/> El ejercicio de tus derechos no incluye ningún dato que el Titular esté obligado a conservar con fines administrativos, legales o de seguridad.
                            </p>
                        </div>
                        <div className="paragraph__mobile">
                            <h3>Cambios en la Política de Privacidad</h3>
                            <p>El Titular se reserva el derecho a modificar la presente Política de Privacidad para adaptarla a novedades legislativas o jurisprudenciales, así como a prácticas de la industria.
                            <br/>Estas políticas estarán vigentes hasta que sean modificadas por otras debidamente publicadas.
                            </p>
                        </div>
                    </div>
                </div>
                <FooterMobile/>
            </div>
            );
        }
        if(isBrowser) {
            return(
                <div className="policy" >
                    <div className="policy__wrapper">
                        <div className="headerTop">
                        <Link to="/">
                            <img className="logo__top" src={require('../components/images/logo/tuprobadorBlanco.png')}/>
                        </Link>
                        </div>
                       
                        <div className="spacer"/>
                        <div className="policy__content" >
                            <h2 id="title">Política de privacidad </h2>
                            <div className="paragraph">
                                <h3>TÉRMINOS LEGALES</h3>
                                <p>ENERGYSOURCE te informa sobre su Política de Privacidad respecto del tratamiento y protección de los datos de carácter personal de los usuarios y clientes que puedan ser recabados por la navegación o contratación de servicios a través del sitio Web tuprobadorapp.com.
                                En este sentido, el Titular garantiza el cumplimiento de la normativa vigente en materia de protección de datos personales, reflejada en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y de Garantía de Derechos Digitales (LOPD GDD). Cumple también con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas (RGPD).
                                El uso de sitio Web implica la aceptación de esta Política de Privacidad así como las condiciones incluidas en el Aviso Legal.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Principios aplicados en el tratamiento de datos</h3>
                                <p>En el tratamiento de tus datos personales, el Titular aplicará los siguientes principios que se ajustan a las exigencias del nuevo reglamento europeo de protección de datos:
                                    {/* <br/>Algunos Servicios le permiten proporcionarnos información directamente. Por ejemplo: */}
                                    <div className="sub-paragraph">
                                        <br/>· Principio de licitud, lealtad y transparencia: El Titular siempre requerirá el consentimiento para el tratamiento de tus datos personales que puede ser para uno o varios fines específicos sobre los que te informará previamente con absoluta transparencia.
                                        <br/>· Principio de minimización de datos: El Titular te solicitará solo los datos estrictamente necesarios para el fin o los fines que los solicita.   
                                        <br/>· Principio de limitación del plazo de conservación: Los datos se mantendrán durante el tiempo estrictamente necesario para el fin o los fines del tratamiento.
                                        <br/>· El Titular te informará del plazo de conservación correspondiente según la finalidad. En el caso de suscripciones, el Titular revisará periódicamente las listas y eliminará aquellos registros inactivos durante un tiempo considerable.  
                                        <br/>· Principio de integridad y confidencialidad: Tus datos serán tratados de tal manera que su seguridad, confidencialidad e integridad esté garantizada.
                                        <br/>· Debes saber que el Titular toma las precauciones necesarias para evitar el acceso no autorizado o uso indebido de los datos de sus usuarios por parte de terceros.
                                    </div>                             
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Obtención de datos personales</h3>
                                <p>Para navegar por tuprobadorapp.com no es necesario que facilites ningún dato personal. <br/>
                                    Los casos en los que sí proporcionas tus datos personales son los siguientes:
                                    <div className="sub-paragraph">
                                        <br/>· Al contactar a través de los formularios de contacto o enviar un correo electrónico.
                                        <br/>· Al realizar un comentario en un artículo o página.
                                        <br/>· Al inscribirte en un formulario de suscripción o un boletín que el Titular gestiona con MailChimp.
                                    </div>           
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Tus derechos</h3>
                                <p>Podemos divulgar tu información personal si se nos requiere por ley o si violas nuestros Términos de Servicio.</p>
                            </div>
                            <div className="paragraph">
                                <h3>Tus derechos</h3>
                                <p>El Titular te informa que sobre tus datos personales tienes derecho a:
                                    <div className="sub-paragraph">
                                        <br/>· Solicitar el acceso a los datos almacenados.
                                        <br/>· Solicitar una rectificación o la cancelación.
                                        <br/>· Solicitar la limitación de su tratamiento.
                                        <br/>· Oponerte al tratamiento.
                                        <br/>· Solicitar la portabilidad de tus datos.
                                    </div>         
                                    <br/>El ejercicio de estos derechos es personal y por tanto debe ser ejercido directamente por el interesado, solicitándolo directamente al Titular, lo que significa que cualquier cliente, suscriptor o colaborador que haya facilitado sus datos en algún momento puede dirigirse al Titular y pedir información sobre los datos que tiene almacenados y cómo los ha obtenido, solicitar la rectificación de los mismos, solicitar la portabilidad de sus datos personales, oponerse al tratamiento, limitar su uso o solicitar la cancelación de esos datos en los ficheros del Titular.
                                    <br/>Para ejercitar tus derechos de acceso, rectificación, cancelación, portabilidad y oposición tienes que enviar un correo electrónico a tuprobador@gmail.com junto con la prueba válida en derecho como una fotocopia del D.N.I. o equivalente.
                                    <br/>Tienes derecho a la tutela judicial efectiva y a presentar una reclamación ante la autoridad de control, en este caso, la Agencia Española de Protección de Datos, si consideras que el tratamiento de datos personales que te conciernen infringe el Reglamento.
                                    <br/>Finalidad del tratamiento de datos personales
                                    <br/>Cuando te conectas al sitio Web para mandar un correo al Titular, te suscribes a su boletín o realizas una contratación, estás facilitando información de carácter personal de la que el responsable es el Titular. Esta información puede incluir datos de carácter personal como pueden ser tu dirección IP, nombre y apellidos, dirección física, dirección de correo electrónico, número de teléfono, y otra información. Al facilitar esta información, das tu consentimiento para que tu información sea recopilada, utilizada, gestionada y almacenada por superadmin.es , sólo como se describe en el Aviso Legal y en la presente Política de Privacidad.
                                    <br/>Los datos personales y la finalidad del tratamiento por parte del Titular es diferente según el sistema de captura de información:
                                    <div className="sub-paragraph">
                                        <br/>· Formularios de contacto: El Titular solicita datos personales entre los que pueden estar: Nombre y apellidos, dirección de correo electrónico, número de teléfono y dirección de tu sitio Web con la finalidad de responder a tus consultas.
                                        <br/>· Por ejemplo, el Titular utiliza esos datos para dar respuesta a tus mensajes, dudas, quejas, comentarios o inquietudes que puedas tener relativas a la información incluida en el sitio Web, los servicios que se prestan a través del sitio Web, el tratamiento de tus datos personales, cuestiones referentes a los textos legales incluidos en el sitio Web, así como cualquier otra consulta que puedas tener y que no esté sujeta a las condiciones del sitio Web o de la contratación.
                                        <br/>· Formularios de suscripción a contenidos: El Titular solicita los siguientes datos personales: Nombre y apellidos, dirección de correo electrónico, número de teléfono y dirección de tu sitio web para gestionar la lista de suscripciones, enviar boletines, promociones y ofertas especiales.
                                        <br/>· Los datos que facilites al Titular estarán ubicados en los servidores de The Rocket Science Group LLC d/b/a, con domicilio en EEUU. (Mailchimp).
                                    </div>    
                                    <br/>Existen otras finalidades por las que el Titular trata tus datos personales:
                                    <div className="sub-paragraph">
                                        <br/>· Para garantizar el cumplimiento de las condiciones recogidas en el Aviso Legal y en la ley aplicable. Esto puede incluir el desarrollo de herramientas y algoritmos que ayuden a este sitio Web a garantizar la confidencialidad de los datos personales que recoge.
                                        <br/>· Para apoyar y mejorar los servicios que ofrece este sitio Web.
                                        <br/>· Para analizar la navegación. El Titular recoge otros datos no identificativos que se obtienen mediante el uso de cookies que se descargan en tu ordenador cuando navegas por el sitio Web cuyas caracterísiticas y finalidad están detalladas en la Política de Cookies .
                                        <br/>· Para gestionar las redes sociales. el Titular tiene presencia en redes sociales. Si te haces seguidor en las redes sociales del Titular el tratamiento de los datos personales se regirá por este apartado, así como por aquellas condiciones de uso, políticas de privacidad y normativas de acceso que pertenezcan a la red social que proceda en cada caso y que has aceptado previamente.
                                    </div>    
                                    <br/>Puedes consultar las políticas de privacidad de las principales redes sociales en estos enlaces:
                                    <div className="sub-paragraph">
                                        <br/>· Facebook
                                        <br/>· Youtube
                                        <br/>· Instagram
                                    </div>   
                                    <br/>El Titular tratará tus datos personales con la finalidad de administrar correctamente su presencia en la red social, informarte de sus actividades, productos o servicios, así como para cualquier otra finalidad que las normativas de las redes sociales permitan. En ningún caso el Titular utilizará los perfiles de seguidores en redes sociales para enviar publicidad de manera individual.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Seguridad de los datos personales</h3>
                                <p>Para proteger tus datos personales, el Titular toma todas las precauciones razonables y sigue las mejores prácticas de la industria para evitar su pérdida, mal uso, acceso indebido, divulgación, alteración o destrucción de los mismos.
                                <br/>El sitio Web está alojado en AWS. La seguridad de tus datos está garantizada, ya que toman todas las medidas de seguridad necesarias para ello. Puedes consultar su política de privacidad para tener más información.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Contenido de otros sitios web</h3>
                                <p>Las páginas de este sitio Web pueden incluir contenido incrustado (por ejemplo, vídeos, imágenes, artículos, etc.). El contenido incrustado de otras web se comporta exactamente de la misma manera que si hubieras visitado la otra web.
                                <br/>Si nuestra tienda es adquirida o fusionada con otra empresa, tu información puede ser transferida a los nuevos propietarios, para que podamos seguir vendiéndote productos.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Política de Cookies</h3>
                                <p>Para que este sitio Web funcione correctamente necesita utilizar cookies, que es una información que se almacena en tu navegador web.
                                <br/>En la página Política de Cookies puedes consultar toda la información relativa a la política de recogida, la finalidad y el tratamiento de las cookies.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Legitimación para el tratamiento de datos</h3>
                                <p>La base legal para el tratamiento de tus datos es: el consentimiento.
                                <br/>Para contactar con el Titular, suscribirte a un boletín o realizar comentarios en este sitio Web tienes que aceptar la presente Política de Privacidad.</p>
                            </div>
                            <div className="paragraph">
                                <h3>Categorías de datos personales</h3>
                                <p>Las categorías de datos personales que trata el Titular son:
                                <div className="sub-paragraph">
                                        <br/>· Datos identificativos.
                                    </div>   
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Conservación de datos personales</h3>
                                <p>Los datos personales que proporciones al Titular se conservarán hasta que solicites su supresión.</p>
                            </div>
                            <div className="paragraph">
                                <h3>Destinatarios de datos personales</h3>
                                <div className="sub-paragraph">
                                        <br/>· Mailchimp The Rocket Science Group LLC d/b/a , con domicilio en EEUU. Encontrarás más información en: https://mailchimp.com. The Rocket Science Group LLC d/b/a trata los datos con la finalidad de prestar sus servicios de email el Titulareting al Titular.
                                        <br/>· Google Analytics es un servicio de analítica web prestado por Google, Inc., una compañía de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos (“Google”). Encontrarás más información en: https://analytics.google.com. Google Analytics utiliza “cookies”, que son archivos de texto ubicados en tu ordenador, para ayudar al Titular a analizar el uso que hacen los usuarios del sitio Web. La información que genera la cookie acerca del uso del sitio Web (incluyendo tu dirección IP) será directamente transmitida y archivada por Google en los servidores de Estados Unidos. También puedes ver una lista de los tipos de cookies que utiliza Google y sus colaboradores y toda la información relativa al uso que hacen de cookies publicitarias.
                                </div>   
                            </div>
                            <div className="paragraph">
                                <h3>Navegación Web</h3>
                                <p>Al navegar por tuprobadorapp.com se pueden recoger datos no identificativos, que pueden incluir, la dirección IP, geolocalización, un registro de cómo se utilizan los servicios y sitios, hábitos de navegación y otros datos que no pueden ser utilizados para identificarte.
                                <br/>El sitio Web utiliza los siguientes servicios de análisis de terceros:
                                <div className="sub-paragraph">
                                        <br/>· Google Analytics
                                </div> 
                                <br/>El Titular utiliza la información obtenida para obtener datos estadísticos, analizar tendencias, administrar el sitio, estudiar patrones de navegación y para recopilar información demográfica.  
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Exactitud y veracidad de los datos personales</h3>
                                <p>Te comprometes a que los datos facilitados al Titular sean correctos, completos, exactos y vigentes, así como a mantenerlos debidamente actualizados.
                                <br/>Como Usuario del sitio Web eres el único responsable de la veracidad y corrección de los datos que remitas al sitio exonerando a el Titular de cualquier responsabilidad al respecto.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Aceptación y consentimiento</h3>
                                <p>Te comprometes a que los datos facilitados al Titular sean correctos, completos, exactos y vigentes, así como a mantenerlos debidamente actualizados.
                                <br/>Como Usuario del sitio Web eres el único responsable de la veracidad y corrección de los datos que remitas al sitio exonerando a el Titular de cualquier responsabilidad al respecto.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Revocabilidad</h3>
                                <p>Para ejercitar tus derechos de acceso, rectificación, cancelación, portabilidad y oposición tienes que enviar un correo electrónico a tuprobado@gmail.com junto con la prueba válida en derecho como una fotocopia del D.N.I. o equivalente.
                                <br/> El ejercicio de tus derechos no incluye ningún dato que el Titular esté obligado a conservar con fines administrativos, legales o de seguridad.
                                </p>
                            </div>
                            <div className="paragraph">
                                <h3>Cambios en la Política de Privacidad</h3>
                                <p>El Titular se reserva el derecho a modificar la presente Política de Privacidad para adaptarla a novedades legislativas o jurisprudenciales, así como a prácticas de la industria.
                                <br/>Estas políticas estarán vigentes hasta que sean modificadas por otras debidamente publicadas.
                                </p>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        }
    }
};
