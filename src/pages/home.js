import React from 'react'
import '../styles/styles.scss';
import { Link } from "react-router-dom";
import { connect } from "react-redux";

//-- COMPONENTS -- desktop
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';
import CustomForm from '../components/subscribeMailForm';

//--3rd party components
import { Player } from 'video-react';
import "../../node_modules/video-react/dist/video-react.css";
import MailchimpSubscribe from "react-mailchimp-subscribe"

import { isBrowser, isMobileOnly} from "react-device-detect";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // url : "//Tuprobador.us3.list-manage.com/subscribe/post?u=e50509d2eeb6f2cd25c6b9af2&id=07d2f88f87",   
            image: require('../styles/images/backgrounds/home.png'),
     
            wallpaper : require('../components/images/landing/wallpaper4.jpg'),
            wallpaper1 : require('../components/images/third/categorias/trousers.png'),
            wallpaper2 : require('../components/images/third/categorias/shirts.png'),
            wallpaper3 : require('../components/images/third/categorias/calzado.png'),
            wallpaper4 : require('../components/images/third/categorias/accesorio.png'),
            categories: ["ropa" , "accesorios" , "calzado"],
        };

        // this.newsLetterSubmit = this.newsLetterSubmit.bind(this);
    }
    displayCategory = (id) => {
        console.log(id);
        this.props.history.push({
            pathname: `/collection/${id}/`,
            // state: { 
            //     trip: Trip,
            // }
        });
    }
    subscribeMailchimp = () => {
        return (
          <MailchimpSubscribe
            url={this.state.url}
            render={({ subscribe, status, message }) => (
              <CustomForm
                status={status}
                message={message}
                onValidated={formData => subscribe(formData)}
              />
            )}
          />
        );
    };

    render() {
    if(isBrowser){
        return (
            <div className="home">
                <Header logged={false}/>
                <div className="homeHeader" >
                    <div className="categoriesWrapper" style={{backgroundImage: `url(${this.state.image})`}} >
                        <div className="categories">
                            <div className="title">
                                <h2>¡Únete a EnergySource!</h2>
                            </div>                            
                            <div className="subtitle">
                                <p>
                                    Disfruta de la energía como nunca antes, <br/>
                                    forma parte de la economía local. 
                                </p>
                            </div>     
                            <div className="buttonWrapper">
                                <button onClick={this.onSubmit}>         
                                    <Link to="/signUp">Regístrate</Link>
                                </button>
                            </div>
                      
                        </div>
                    </div>
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
              
                <div className="newsletterCallToAction" /*style={{backgroundImage: `url(${this.state.wallpaper})`}}*/>
                    <div className="newsLetterTitle">
                        <div className="title">
                            <h2>¡Únete a EnergySource!</h2>
                        </div>
                        {/* <div className="title">
                            <h2> a Tuprobador </h2>
                        </div> */}
                        {/* <div className="title">
                            <h2> SHARRYUP </h2>
                        </div> */}
                    </div>
                    <div className="newsLetterSubtitle">
                        <br/>
                        <div className="subtitle">
                            <p>Disfruta de la energía a nuestro alcance   </p>
                        </div>
                        <div className="subtitle">
                            <p> como nunca antes.</p>
                        </div>

                        {/* <div className="subtitle">
                            <p id="bold">Déjanos el email ahora. </p>
                        </div> */}
                        
                    </div>
                    <div className="newsLetterInput">
                        {/* <MailchimpSubscribe placeholder="info@sharryup.com" type="email" url={"this.state.url"} classname="gotIt"/> */}
                        {this.subscribeMailchimp()}
                        {/* <input onChange={this.hendleMailInput} placeholder="info@sharryup.com" type="email" id="newsLetterMail"></input><button onClick={this.newsLetterSubmit}>FORMAR PARTE</button> */}
                    </div>
                    {/* <div className={this.state.error.error?"newsLetterInputError":"newsLetterInputSent"}>
                        {this.state.error.display?this.state.error.value:null}
                    </div> */}
                    <div className="newsLetterSubtitle">
                        
                        <div className="socialMediaIcons">
                            <Link to="https://www.instagram.com/" target="_blank"> <img id="insta" src={require("../components/images/footer/instagram_icon.png")}/> </Link>
                            <Link to="https://www.facebook.com/" target="_blank"> <img id="fb" src={require("../components/images/footer/facebook-logo@3x.png")}/> </Link>
                            <Link to="https://www.youtube.com/" target="_blank"> <img id="ytb" className="youtube__icon" src={require("../components/images/footer/youtube-play-button@3x.png")}/> </Link>
                        </div>
                    </div>
                   
                </div>
                <Footer/>
            </div>
        )
    }
    if(isMobileOnly) {
        return (
            <div className="homeScreenMobile" style={{backgroundImage: `url(${this.state.image})`}}>
                    <HeaderMobile logged={false}/>
                    {/* <div className="logoLandingMobile">
                        hello */}
                        {/* <img  src={require('../components/images/logo/tuprobadorBlanco.png')}/> */}
                    {/* </div> */}
                    <div className="homeMobile__header" >
                        {/* <Player
                            playsInline
                            poster={require("../components/images/landing/captura.png")}
                            src="https://s3.eu-west-3.amazonaws.com/media.tuprobadorapp.com/3eb2c59a-1a24-4469-ad44-445cb57a6154.MP4"
                        /> */}
                            <div className="title">
                                <h2>¡Únete a EnergySource!</h2>
                            </div>
                            <div className="title">
                                <p>
                                    Disfruta de la energía como nunca antes, <br/>
                                    forma parte de la economía local. 
                                </p>
                            </div>
                            <div className="buttonWrapper">
                                <button onClick={this.onSubmit}>         
                                    <Link to="/signUp">Regístrate</Link>
                                </button>
                            </div>
                    </div> 
                    <div className="newsletterCallToActionMobile" >
                        <div className="newsLetterTitleMobile">
                            <div className="title">
                                <h2>¡Únete a EnergySource!</h2>
                            </div>
                            <div className="title">
                                <p>
                                    Disfruta de la energía como nunca antes, <br/>
                                    forma parte de la economía local. 
                                </p>
                            </div>
                            <div className="buttonWrapper">
                                <button onClick={this.onSubmit}>         
                                    <Link to="/signUp">Regístrate</Link>
                                </button>
                            </div>
                        </div>
                        <div className="newsLetterSubtitleMobilee">
                            <br/>
                            <div className="subtitle">
                                <p>Disfruta de la energía a nuestro alcance   </p>
                            </div>
                            <div className="subtitle">
                                <p> como nunca antes.</p>
                            </div>
                            {/* <div className="subtitle">
                                <p id="bold">Déjanos el email ahora. </p>
                            </div> */}
                            
                        </div>
                        <div className="newsLetterInputMobile">
                            {this.subscribeMailchimp()}
                            {/* <input onChange={this.hendleMailInput} placeholder="info@sharryup.com" type="email" id="newsLetterMail"></input><button onClick={this.newsLetterSubmit}>FORMAR PARTE</button> */}
                        </div>
                       

                        </div>  
                        <FooterMobile/> 

                   
                </div>
        )
    }
  }
}


export default (Home);