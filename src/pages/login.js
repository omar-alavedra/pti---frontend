//--This file shows an example of a Login class component that works as a Login modal and uses the previous Redux functions shown.
//--the Redux functions are described in this files "/actions.js" "/authReducer.js" and "/Auth.js"
import React from 'react';
import { authSignin, authGetContracts, authGetUserInfo} from '../store/actions/index';  
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { isBrowser, isMobileOnly} from "react-device-detect";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
        redirect: false,
        image: require('../styles/images/backgrounds/signUp.png'),
        controls: {
            email: {
                value: "",
                valid: false,
                validationRules: {
                    isEmail: true
                }
            },
            password: {
                value: "",
                valid: false,
                validationRules: {
                    minLength: 4
                }
            }
        }
    };

  }

  validateForm = (typedText) => {
    return this.state.controls.email.length > 0 && this.state.controls.password.length > 0;
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.authSignin(this.state.controls);
    if(this.props.logged) {
        this.setState({redirect: true}, this.props.authGetContracts(this.props.token), this.props.authGetUserInfo(this.props.token)
        );
    }
  }

  handleChange = (e) => {
    const event = e;
    const id = event.target.id;
    const val = event.target.value;
    
    this.setState(prevState => {
        return {
            controls: {
                ...prevState.controls,
                [id]: {
                    ...prevState.controls[id],
                    value: val,
                },
            }
        };
    });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect push to="/user" />;
    }
    if(isBrowser){
        return (
            <div className="login" style={{backgroundImage: `url(${this.state.image})`}}>
                <div className="login__wrapper">
                    <div className="login__dialog">
                        <div className="login__dialog__wrapper" >
                            {/* <img id="logo" src={require('./images/logo/logo-sharryup_color.png')}/> */}
                            <div className="login__input">
                                <h4>Login</h4>
                                <form
                                    onSubmit={this.handleSubmit}
                                    onChange={this.handleTextInput}
                                    className="tripForm__wrapper"
                                >
                                    <div className="input__wrapper">
                                            <a>Email</a>
                                            <input
                                            id="email"
                                            type="email"
                                            value={this.state.controls.email.value}
                                            onChange={this.handleChange}
                                            />
                                    </div>
                                    <div className="input__wrapper">
                                            <a>Password</a>
                                            <input
                                            id="password"
                                            value={this.state.controls.password.value}
                                            onChange={this.handleChange}
                                            type="password"
                                            />                              
                                    </div>
                                </form>
                                <button onClick={this.onSubmit}> Login </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    else if(isMobileOnly){
        return (
            <div className="loginMobile" style={{backgroundImage: `url(${this.state.image})`}}>
                <div className="login__wrapper">
                    <div className="login__dialog">
                        <div className="login__dialog__wrapper" >
                            {/* <img id="logo" src={require('./images/logo/logo-sharryup_color.png')}/> */}
                            <div className="login__input">
                                <h4>Login</h4>
                                <form
                                    onSubmit={this.handleSubmit}
                                    onChange={this.handleTextInput}
                                    className="tripForm__wrapper"
                                >
                                    <div className="input__wrapper">
                                            <a>Email</a>
                                            <input
                                            id="email"
                                            type="email"
                                            value={this.state.controls.email.value}
                                            onChange={this.handleChange}
                                            />
                                    </div>
                                    <div className="input__wrapper">
                                            <a>Password</a>
                                            <input
                                            id="password"
                                            value={this.state.controls.password.value}
                                            onChange={this.handleChange}
                                            type="password"
                                            />                              
                                    </div>
                                </form>
                                <button onClick={this.onSubmit}> Login </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    
  }
}

//--through the function mapDispatchToProps we can allow this component to use the functions specified in the returned object, in this ase authSignin.--

const mapDispatchToProps = dispatch => {
    return {
        authSignin: (authData) => dispatch(authSignin(authData)),
        authGetContracts: (authData) => dispatch(authGetContracts(authData)),
        authGetUserInfo: (authData) => dispatch(authGetUserInfo(authData)),
    }
}

//--through the function mapDispatchToProps we can allow this component to use the state parameters specified in the returned object.--


const mapStateToProps = state => {
    return {
        token: state.authReducer.serverState.TOKEN_LOGIN,
        server: state.server,
        error: state.authReducer.serverState.errorMessage,
        logged: state.authReducer.serverState.logged
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);
// export default (Login);