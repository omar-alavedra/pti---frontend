import React from 'react'
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Redirect } from 'react-router-dom';


import { authSignup } from '../store/actions/index';

//-- COMPONENTS -- desktop
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';

//--3rd party components
import { isBrowser, isMobileOnly} from "react-device-detect";
import Select from 'react-select';
import * as EmailValidator from 'email-validator';
var passwordValidator = require('password-validator');

//Schema for validating the passwords
var schema = new passwordValidator();
//the rules for the password validation
schema
.is().min(8)                                    // Minimum length 8
.is().max(100)                                  // Maximum length 100
.has().uppercase()                              // Must have uppercase letters
.has().lowercase()                              // Must have lowercase letters
.has().digits()                                 // Must have digits
.has().not().spaces()                           // Should not have spaces

//Redux functions
import {deleteFromCart } from '../store/actions/index';


class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            category: this.props.match.params.category,
            redirectCheckout: false,
            signIn: false,
            image: require('../styles/images/backgrounds/signUp.png'),
            errors: {
                name: {
                    display: false,
                    stat: true,
                    val: 'Debe contener almenos 2 carácteres.',
                },
                lastName: {
                    display: false,
                    stat: true,
                    val: 'Debe contener almenos 2 carácteres.',
                },
                email:{
                    display: false,
                    stat: true,
                    val: 'Email inválido.',
                },
                recoveryEmail:{
                    display: false,
                    stat: true,
                    val: 'Email inválido.',
                },
                password:{
                    display: false,
                    stat: true,
                    val: 'Contrasenya: min. 8 carácteres, mayúscula, minúsucla, carácter alfanumérico.'
                },
                signIn:{
                    display: false,
                    stat: true,
                    val: 'Usuario / contrasenya incorrecto/s.',
                },
                birthday:{
                    display: false,
                    stat: true,
                    val: 'Fecha incompleta.'
                }
            },
            birthday: {
                daySelector: {
                    stat: false,
                    val: ''
                },
                monthSelector: {
                    stat: false,
                    val: ''
                },
                yearSelector: {
                    stat: false,
                    val: ''
                },
            },
            days : [
                { value: '01', label: '1' , id:"daySelector"},
                { value: '02', label: '2' , id:"daySelector"},
                { value: '03', label: '3' , id:"daySelector"},
                { value: '04', label: '4' , id:"daySelector"},
                { value: '05', label: '5' , id:"daySelector"},
                { value: '06', label: '6' , id:"daySelector"},
                { value: '07', label: '7' , id:"daySelector"},
                { value: '08', label: '8' , id:"daySelector"},
                { value: '09', label: '9' , id:"daySelector"},
                { value: '10', label: '10' , id:"daySelector"},
                { value: '11', label: '11' , id:"daySelector"},
                { value: '12', label: '12' , id:"daySelector"},
                { value: '13', label: '13' , id:"daySelector"},
                { value: '14', label: '14' , id:"daySelector"},
                { value: '15', label: '15' , id:"daySelector"},
                { value: '16', label: '16' , id:"daySelector"},
                { value: '17', label: '17' , id:"daySelector"},
                { value: '18', label: '18' , id:"daySelector"},
                { value: '19', label: '19' , id:"daySelector"},
                { value: '20', label: '20' , id:"daySelector"},
                { value: '21', label: '21' , id:"daySelector"},
                { value: '22', label: '22' , id:"daySelector"},
                { value: '23', label: '23' , id:"daySelector"},
                { value: '24', label: '24' , id:"daySelector"},
                { value: '25', label: '25' , id:"daySelector"},
                { value: '26', label: '26' , id:"daySelector"},
                { value: '27', label: '27' , id:"daySelector"},
                { value: '28', label: '28' , id:"daySelector"},
                { value: '29', label: '29' , id:"daySelector"},
                { value: '30', label: '30' , id:"daySelector"},
                { value: '31', label: '31' , id:"daySelector"},

              ],
            months: [                
                { value: '01', label: 'Enero' , id:"monthSelector"},
                { value: '02', label: 'Febrero' , id:"monthSelector"},
                { value: '03', label: 'Marzo' , id:"monthSelector"},
                { value: '04', label: 'Abril' , id:"monthSelector"},
                { value: '05', label: 'Mayo' , id:"monthSelector"},
                { value: '06', label: 'Junio' , id:"monthSelector"},
                { value: '07', label: 'Julio' , id:"monthSelector"},
                { value: '08', label: 'Agosto' , id:"monthSelector"},
                { value: '09', label: 'Septiempre' , id:"monthSelector"},
                { value: '10', label: 'Octubre' , id:"monthSelector"},
                { value: '11', label: 'Noviembre' , id:"monthSelector"},
                { value: '12', label: 'Diciembre' , id:"monthSelector"},
            ],
            years: [                
                // { value: '2019', label: '2019' , id:"yearSelector"},
                { value: '2018', label: '2018' , id:"yearSelector"},
                { value: '2017', label: '2017' , id:"yearSelector"},
                { value: '2016', label: '2016' , id:"yearSelector"},
                { value: '2015', label: '2015' , id:"yearSelector"},
                { value: '2014', label: '2014' , id:"yearSelector"},
                { value: '2013', label: '2013' , id:"yearSelector"},
                { value: '2012', label: '2012' , id:"yearSelector"},
                { value: '2011', label: '2011' , id:"yearSelector"},
                { value: '2010', label: '2010' , id:"yearSelector"},
                { value: '2009', label: '2009' , id:"yearSelector"},
                { value: '2008', label: '2008' , id:"yearSelector"},
                { value: '2007', label: '2007' , id:"yearSelector"},
                { value: '2006', label: '2006' , id:"yearSelector"},
                { value: '2005', label: '2005' , id:"yearSelector"},
                { value: '2004', label: '2004' , id:"yearSelector"},
                { value: '2003', label: '2003' , id:"yearSelector"},
                { value: '2002', label: '2002' , id:"yearSelector"},
                { value: '2001', label: '2001' , id:"yearSelector"},
                { value: '2000', label: '2000' , id:"yearSelector"},
                { value: '1999', label: '1999' , id:"yearSelector"},
                { value: '1998', label: '1998' , id:"yearSelector"},
                { value: '1997', label: '1997' , id:"yearSelector"},
                { value: '1996', label: '1996' , id:"yearSelector"},
                { value: '1995', label: '1995' , id:"yearSelector"},
                { value: '1994', label: '1994' , id:"yearSelector"},
                { value: '1993', label: '1993' , id:"yearSelector"},
                { value: '1992', label: '1992' , id:"yearSelector"},
                { value: '1991', label: '1991' , id:"yearSelector"},
                { value: '1990', label: '1990' , id:"yearSelector"},
                { value: '1989', label: '1989' , id:"yearSelector"},
                { value: '1988', label: '1988' , id:"yearSelector"},
                { value: '1987', label: '1987' , id:"yearSelector"},
                { value: '1986', label: '1986' , id:"yearSelector"},
                { value: '1985', label: '1985' , id:"yearSelector"},
                { value: '1984', label: '1984' , id:"yearSelector"},
                { value: '1983', label: '1983' , id:"yearSelector"},
                { value: '1982', label: '1982' , id:"yearSelector"},
                { value: '1981', label: '1981' , id:"yearSelector"},
                { value: '1980', label: '1980' , id:"yearSelector"},
                { value: '1979', label: '1979' , id:"yearSelector"},
                { value: '1978', label: '1978' , id:"yearSelector"},
                { value: '1977', label: '1977' , id:"yearSelector"},
                { value: '1976', label: '1976' , id:"yearSelector"},
                { value: '1975', label: '1975' , id:"yearSelector"},
                { value: '1974', label: '1974' , id:"yearSelector"},
                { value: '1973', label: '1973' , id:"yearSelector"},
                { value: '1972', label: '1972' , id:"yearSelector"},
                { value: '1971', label: '1971' , id:"yearSelector"},
                { value: '1970', label: '1970' , id:"yearSelector"},
                { value: '1969', label: '1969' , id:"yearSelector"},
                { value: '1968', label: '1968' , id:"yearSelector"},
                { value: '1967', label: '1967' , id:"yearSelector"},
                { value: '1966', label: '1966' , id:"yearSelector"},
                { value: '1965', label: '1965' , id:"yearSelector"},
                { value: '1964', label: '1964' , id:"yearSelector"},
                { value: '1963', label: '1963' , id:"yearSelector"},
                { value: '1962', label: '1962' , id:"yearSelector"},
                { value: '1961', label: '1961' , id:"yearSelector"},
                { value: '1960', label: '1960' , id:"yearSelector"},
                { value: '1959', label: '1959' , id:"yearSelector"},
                { value: '1958', label: '1958' , id:"yearSelector"},
                { value: '1957', label: '1957' , id:"yearSelector"},
                { value: '1956', label: '1956' , id:"yearSelector"},
                { value: '1955', label: '1955' , id:"yearSelector"},
                { value: '1954', label: '1954' , id:"yearSelector"},
                { value: '1953', label: '1953' , id:"yearSelector"},
                { value: '1952', label: '1952' , id:"yearSelector"},
                { value: '1951', label: '1951' , id:"yearSelector"},
                { value: '1950', label: '1950' , id:"yearSelector"},
                { value: '1949', label: '1949' , id:"yearSelector"},
                { value: '1948', label: '1948' , id:"yearSelector"},
                { value: '1947', label: '1947' , id:"yearSelector"},
                { value: '1946', label: '1946' , id:"yearSelector"},
                { value: '1945', label: '1945' , id:"yearSelector"},
                { value: '1944', label: '1944' , id:"yearSelector"},
                { value: '1943', label: '1943' , id:"yearSelector"},
                { value: '1942', label: '1942' , id:"yearSelector"},
                { value: '1941', label: '1941' , id:"yearSelector"},
                { value: '1940', label: '1940' , id:"yearSelector"},
            ],
            user: {
                name: '',
                lastName: '',
                fullName: '',
                email: '',
                birthday: '',
                password: '',
            },
            controls: {
                email: {
                    value: "",
                    valid: false,
                    validationRules: {
                        isEmail: true
                    }
                },
                password: {
                    value: "",
                    valid: false,
                    validationRules: {
                        minLength: 4
                    }
                },
                recoveryEmail: {
                    value: "",
                    valid: false,
                    validationRules: {
                        isEmail: true
                    }
                },
            },
        };
        this.handleTextInput = this.handleTextInput.bind(this);
        this.handleSelectedOption = this.handleSelectedOption.bind(this);
        this.signUp = this.signUp.bind(this);
    }

    // checkoutThirdStep = () => {
    //     this.setState({
    //         ...this.state,
    //         redirectCheckout: true
    //     })
    // }
    activate(){
        if(!this.state.errors.name.stat && !this.state.errors.lastName.stat && !this.state.errors.password.stat && !this.state.errors.email.stat && !this.state.errors.birthday.stat){
            return true;
        }
        else return false;
    }
    handleTextInput(e){
        const event = e;
        const id = event.target.id;
        const val = event.target.value;
        //console.log(id);
        // console.log(val);
        // console.log(this.state.errors[id].val)
        var invalid = true;
        var fullname
        if(id==="name" || id==="lastName"){
            if(val.length >= 2) {
                //correct
                invalid = false;
                // if(id === "name"){
                //     fullname = `${val} ${this.state.user.lastName}`
                // }
                // else{
                //     fullname = `${this.state.user.name} ${val}`
                // }
                // this.setState({
                //     user:{
                //         ...this.state.user,
                //         fullName: fullname
                //     }
                // })
            }
            else{
                //incorrect
            }
        }
        else if(id === "email"){
            var valid = false;
            valid = EmailValidator.validate(val);
            if(valid){
                //correct
                invalid=false;
            }
            else {
                //incorrect
            }
        }
        else if(id === "password"){
            var valid = false;
            valid = schema.validate(val)
            if(valid){
                //correct
                invalid=false;
            }
            else {
                //incorrect
            }
        }
        else if(id === "emailSignIn"){
            var valid = false;
            valid = EmailValidator.validate(val);
            if(valid){
                //correct
                invalid=false;
                this.setState({
                    controls: {
                        ...this.state.controls,
                        email: {
                            ...this.state.controls.email,
                            value: val,
                            valid: true,
                            validationRules: {
                                isEmail: true
                            }
                        }
                    }
                });
            }
            else {
                //incorrect
            }
        }
        else if(id === "passwordSignIn"){
            invalid=false;
            this.setState({
                controls: {
                    ...this.state.controls,
                    password: {
                        ...this.state.controls.password,
                        value: val,
                        valid: this.state.controls.password.valid,
                        validationRules: this.state.controls.password.validationRules

                    }
                }
            });
        }

        if(invalid && (id !== "passwordSignIn" && id !== "emailSignIn")){
            this.setState({
                errors: {
                    ...this.state.errors,
                    [id]: {
                        display: true,
                        stat: true,
                        val: this.state.errors[id].val,
                    },
                }
            }
            );
        }
        else {
            if(id === "passwordSignIn"|| id === "emailSignIn"){
                // this.setState({
                //     user: {
                //         ...this.state.user,
                //         [id]: val,
                //     }
                // });
                this.setState({
                    errors: {
                        ...this.state.errors,
                        signIn: {
                            display: false,
                            stat: false,
                            val: this.state.errors.signIn.val,
                        },
                    }
                }
                );
            }
            else{
                this.setState({
                    user: {
                        ...this.state.user,
                        [id]: val,
                    }
                });
                this.setState({
                    errors: {
                        ...this.state.errors,
                        [id]: {
                            display: false,
                            stat: false,
                            val: this.state.errors[id].val,
                        },
                    }
                }
                );
            }
            
        }
    }
    handleSelectedOption(e){
        // console.log(e);
        // console.log(this.state.birthday)
        if(e.id === "daySelector"||e.id === "monthSelector"||e.id === "yearSelector"){
            this.setState({
                birthday:{
                    ...this.state.birthday,
                    [e.id]: {
                        stat: true,
                        val: e.value
                    }
                }
            })
        }
        var validDate = false;
        var birthday;
        if(e.id === "daySelector"){
            if(this.state.birthday.monthSelector.stat && this.state.birthday.yearSelector.stat){
                validDate=true;
                birthday = `${e.value} ${this.state.birthday.monthSelector.val} ${this.state.birthday.yearSelector.val}`
                this.setState({
                    user:{
                        ...this.state.user,
                        birthday: birthday
                    }
                })
            }
        }
        else if(e.id === "monthSelector"){
            if(this.state.birthday.daySelector.stat && this.state.birthday.yearSelector.stat){
                validDate=true;
                birthday = `${this.state.birthday.daySelector.val} ${e.value} ${this.state.birthday.yearSelector.val}`
                this.setState({
                    user:{
                        ...this.state.user,
                        birthday: birthday
                    }
                })
            }
        }
        else if(e.id === "yearSelector"){
            if(this.state.birthday.monthSelector.stat && this.state.birthday.daySelector.stat){
                validDate=true;
                birthday = `${this.state.birthday.daySelector.val} ${this.state.birthday.monthSelector.val} ${e.value}`
                // console.log(birthday)
                this.setState({
                    user:{
                        ...this.state.user,
                        birthday: birthday
                    }
                })
            }
        }
        if(validDate){
            //console.log("valid")
            this.setState({
                errors: {
                    ...this.state.errors,
                    birthday: {
                        display: false,
                        stat: false,
                        val: this.state.errors.birthday.val,
                    },
                }
            });
        }
        else{
            this.setState({
                errors: {
                    ...this.state.errors,
                    birthday: {
                        display: true,
                        stat: true,
                        val: this.state.errors.birthday.val,
                    },
                }
            });
        }
    }
    signUp(e){
        e.preventDefault();
        //console.log(this.validateInputs());
        // this.setUpParams();
        var user = this.state.user
        user.fullName = `${this.state.user.name} ${this.state.user.lastName}`
        // console.log(user)
        this.props.authSignup(user)
        this.setState({
            signIn: true
        })
        // this.props.authSignup(user)
        // this.setState({
        //     signUpInitial:false,
        //     signUpByMail:false,
        //     signIn:false,
        //     goodToGo: true,
        //     goodToGoLogIn: false,
        // })
    }
    render() {

        if (this.state.signIn) {
            return <Redirect to='/login/'/>;

        }
        if(isBrowser){
            return(
                <div className="signModalWeb" style={{backgroundImage: `url(${this.state.image})`}}>
                    {/* <div className="signModalBackground" onClick={()=>this.dismiss()}>
                        background
                    </div>       */}
                    {/* <Fade>                  */}
                    <div className="signModalWrapperTallWeb">
                    {/* <button id="dismiss" onClick={()=>this.dismiss()}><img src={require('./images/icons/ic_dismiss.png')}></img></button> */}
                        <div className="signModalContainer">
                            <p>Regístrate para unirte</p>
                            <div className="modalInputs">
                                <div className="modalInputContainer">
                                    <input className={this.state.errors.name.stat?"invalidInput":"validInput"} id="name" placeholder="Nombre" type="text" onChange={this.handleTextInput}></input>
                                    {/* <Fade> */}
                                        <a className={this.state.errors.name.display?"invalid":"valid"} >{this.state.errors.name.val}</a>
                                    {/* </Fade> */}
                                </div>
                                <div className="modalInputContainer">
                                    <input className={this.state.errors.lastName.stat?"invalidInput":"validInput"} id="lastName" placeholder="Apellidos" type="text" onChange={this.handleTextInput}></input>
                                    {/* <Fade> */}
                                        <a className={this.state.errors.lastName.display?"invalid":"valid"} >{this.state.errors.lastName.val}</a>
                                    {/* </Fade> */}
                                </div>
                                <div className="modalInputContainer">
                                    <input className={this.state.errors.email.stat?"invalidInput":"validInput"} id="email" placeholder="Email" type="email" onChange={this.handleTextInput}></input>
                                    {/* <Fade> */}
                                        <a className={this.state.errors.email.display?"invalid":"valid"} >{this.state.errors.email.val}</a>
                                    {/* </Fade> */}
                                </div>
                                <div className="modalInputContainer">
                                    <input className={this.state.errors.password.stat?"invalidInput":"validInput"} id="password" placeholder="Password" type="password" onChange={this.handleTextInput}></input>
                                    {/* <Fade> */}
                                        <a className={this.state.errors.password.display?"invalid":"valid"} >{this.state.errors.password.val}</a>
                                    {/* </Fade> */}
                                </div>
                                <div className="modalInputContainer">
                                    <a id="birthday" onChange={this.handleTextInput}>Fecha de nacimiento</a>  
                                    {/* <Fade> */}
                                        <a className={this.state.errors.birthday.display?"invalid":"valid"} >{this.state.errors.birthday.val}</a>
                                    {/* </Fade> */}
                                </div>
                                <div className="birthdayContainer">
                                    <Select id="daySelector" menuPlacement="top" placeholder="Dia" onChange={this.handleSelectedOption} options={this.state.days} className="selectorBirthday"/>
                                    <Select id="monthSelector"  menuPlacement="top" placeholder="Mes" onChange={this.handleSelectedOption} options={this.state.months} className="selectorBirthday"/>
                                    <Select id="yearSelector" menuPlacement="top" placeholder="Año" onChange={this.handleSelectedOption} options={this.state.years} className="selectorBirthday"/>
                                </div>
                                <div className="signUpButtonContainer">
                                    <button disabled={!this.activate()} id="signUpButton" onClick={this.signUp}>Registrarse</button>
                                    {/* <button  onClick={this.signUp}>Registrarse</button> */}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* </Fade> */}
                </div>
            )
        }
        if(isMobileOnly) {
            return (
                <div className="signModalMobile" style={{backgroundImage: `url(${this.state.image})`}}>
                {/* <div className="signModalBackground" onClick={()=>this.dismiss()}>
                    background
                </div>       */}
                {/* <Fade>                  */}
                <div className="signModalWrapperTallMobile">
                {/* <button id="dismiss" onClick={()=>this.dismiss()}><img src={require('./images/icons/ic_dismiss.png')}></img></button> */}
                    <div className="signModalContainer">
                        <p>Regístrate para unirte</p>
                        <div className="modalInputs">
                            <div className="modalInputContainer">
                                <input className={this.state.errors.name.stat?"invalidInput":"validInput"} id="name" placeholder="Nombre" type="text" onChange={this.handleTextInput}></input>
                                {/* <Fade> */}
                                    <a className={this.state.errors.name.display?"invalid":"valid"} >{this.state.errors.name.val}</a>
                                {/* </Fade> */}
                            </div>
                            <div className="modalInputContainer">
                                <input className={this.state.errors.lastName.stat?"invalidInput":"validInput"} id="lastName" placeholder="Apellidos" type="text" onChange={this.handleTextInput}></input>
                                {/* <Fade> */}
                                    <a className={this.state.errors.lastName.display?"invalid":"valid"} >{this.state.errors.lastName.val}</a>
                                {/* </Fade> */}
                            </div>
                            <div className="modalInputContainer">
                                <input className={this.state.errors.email.stat?"invalidInput":"validInput"} id="email" placeholder="Email" type="email" onChange={this.handleTextInput}></input>
                                {/* <Fade> */}
                                    <a className={this.state.errors.email.display?"invalid":"valid"} >{this.state.errors.email.val}</a>
                                {/* </Fade> */}
                            </div>
                            <div className="modalInputContainer">
                                <input className={this.state.errors.password.stat?"invalidInput":"validInput"} id="password" placeholder="Password" type="password" onChange={this.handleTextInput}></input>
                                {/* <Fade> */}
                                    <a className={this.state.errors.password.display?"invalid":"valid"} >{this.state.errors.password.val}</a>
                                {/* </Fade> */}
                            </div>
                            <div className="modalInputContainer">
                                <a id="birthday" onChange={this.handleTextInput}>Fecha de nacimiento</a>  
                                {/* <Fade> */}
                                    <a className={this.state.errors.birthday.display?"invalid":"valid"} >{this.state.errors.birthday.val}</a>
                                {/* </Fade> */}
                            </div>
                            <div className="birthdayContainer">
                                <Select id="daySelector" menuPlacement="top" placeholder="Dia" onChange={this.handleSelectedOption} options={this.state.days} className="selectorBirthday"/>
                                <Select id="monthSelector"  menuPlacement="top" placeholder="Mes" onChange={this.handleSelectedOption} options={this.state.months} className="selectorBirthday"/>
                                <Select id="yearSelector" menuPlacement="top" placeholder="Año" onChange={this.handleSelectedOption} options={this.state.years} className="selectorBirthday"/>
                            </div>
                            <div className="signUpButtonContainer">
                                <button disabled={!this.activate()} id="signUpButton" onClick={this.signUp}>Registrarse</button>
                                {/* <button  onClick={this.signUp}>Registrarse</button> */}
                            </div>
                        </div>
                    </div>
                </div>
                {/* </Fade> */}
            </div>
            )
        }
    }
}

const mapStateToProps = (state)=>{
    // console.log(state)
    return {
        // items: state.cartReducer.addedItems,
        // category: state.storeReducer.category
    }
}

const mapDispatchToProps= (dispatch)=>{
    return{
        authSignup: (authData) => dispatch(authSignup(authData)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(SignUp);