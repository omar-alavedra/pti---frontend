import React from 'react';
import { Redirect } from 'react-router';

const NotFound = () => {
    return(
        <div>
            <Redirect push to="/" />
        </div>
    )
}

export default NotFound;