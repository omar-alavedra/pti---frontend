import React from 'react'
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Redirect } from 'react-router-dom';

//-- COMPONENTS -- desktop
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';


//--3rd party components
import { isBrowser, isMobileOnly} from "react-device-detect";

//Redux functions
// import {deleteFromCart } from '../store/actions/index';
import {getContracts } from '../store/actions/index';
import moment from "moment";
const axios = require('axios');


class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            contract: false,
            information:[],
            options: {
                selected: 'home'
            },
            user: {
                name: "John",
                lastName: "Appleseed",
                country: "Spain",
                walletID: 123456789,
                mail: "example@mail.com",
                contract: false
            },
            wallet: {
                balance: 120.4,
                price: 4,
                id: 123456789,
            },
            contractSelected:'none',
            contracts: [
                {
                    id:0,
                    provider: "Tanner",
                    tipo_energia: "eolica",
                    fecha_inicio: "1/12/2019",
                    fecha_fin: "31/12/2019",
                    potencia_subministrada: "600",
                    precio: "0.025"
                },
            //     {
            //         id:1,
            //         provider: "Melissa",
            //         tipo_energia: "solar",
            //         fecha_inicio: "1/12/2019",
            //         fecha_fin: "31/12/2019",
            //         potencia_subministrada: "500",
            //         precio: "0.035"
            //     },
            //     {
            //         id:2,
            //         provider: "Joan",
            //         tipo_energia: "solar",
            //         fecha_inicio: "1/12/2019",
            //         fecha_fin: "31/12/2019",
            //         potencia_subministrada: "400",
            //         precio: "0.025"
            //     },
            ]
        };
        this.contract = this.contract.bind(this);
        this.selectedOption = this.selectedOption.bind(this);
    }

    
    contract = (id, contractInfo) => {
        console.log(id)

        this.setState({
            contract: true,
            contractSelected: contractInfo,
        })

        
    }

    selectedOption = (e) => {
        if(e==='information'){
            console.log(this.props.user)
            var self = this
            axios.post('https://cors-anywhere.herokuapp.com/http://34.70.95.68:3000/api/contract/listOwnContract',{'auth-token':this.props.token}, {headers: {'auth-token':this.props.token, 'Content-Type': 'application/json', "Accept": "application/json", } } )
            .then(function (response) {
              console.log(response);
              // this.props.getContracts(response)
            //   dispatch(saveContracts(response.data));
            self.setState({
                information: response.data
            })
            // write = true
            // information=response.data
            
        })
            .catch(function (error) {
              console.log(error);
            });
        }
        this.setState({
            ...this.state,
            options: {
                ...this.state.options,
                selected: e
            }
        })
    }
    marketplace = () => {
        console.log(this.props.contracts)
        if(this.props.contracts){

        
        if(isBrowser){
            return(
                <div className="marketPlace">
                    <div className="container">
                        <div className="title">
                            <p>Marketplace</p>
                        </div>
                        <div className="content">
                            {this.props.contracts.map((contract,index) => {
                                console.log(contract.creation_date)
                                var t = new Date(1965,0,1)
                                t.setSeconds(contract.creation_date)
                                return(
                                    <div className="contractCard" key={index}>
                                        <div className="information">
                                            <div>
                                                <p>Proveedor: {contract.id_vendedor}</p>
                                                <p>Energia: {contract.energy_type}</p>
                                                <p>Fecha inicio: {moment(t).format("DD/MM/YY")}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {contract.potencia} kW</p>
                                                <p>Precio: {contract.precio} €/kWh</p>
                                                <p>Permanencia: {contract.permanecia} días</p>
                                            </div>
                                        </div>
                                        <button className="buttonContainer" onClick={()=>this.contract(contract._id, contract)}>CONTRATAR</button>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            )
        }
        else if(isMobileOnly) {
        return(
            <div className="marketPlaceMobile">
                <div className="container">
                    <div className="title">
                        <p>Marketplace</p>
                    </div>
                    <div className="content">
                        {this.props.contracts.map((contract,index) => {
                              var t = new Date(1965,0,1)
                              t.setSeconds(contract.creation_date)
                            return(
                                <div className="contractCard" key={index}>
                                        <div className="information">
                                            <div>
                                                <p>Proveedor: {contract.id_vendedor}</p>
                                                <p>Energia: {contract.energy_type}</p>
                                                <p>Fecha inicio: {moment(t).format("DD/MM/YY")}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {contract.potencia} kW</p>
                                                <p>Precio: {contract.precio} €/kWh</p>
                                                <p>Permanencia: {contract.permanecia} días</p>
                                            </div>
                                        </div>
                                        <button className="buttonContainer" onClick={()=>this.contract(contract._id, contract)}>CONTRATAR</button>
                                    </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
                    }
                }
    }

    wallet = () => {
        // console.log(this.props)
        
        if(isBrowser){
            return(
                <div className="wallet">
                    <div className="container">
                        <div className="title">
                            <p>My balance</p>
                        </div>
                        <div className="content">
                            <p>SourceCoin: {this.state.wallet.balance*this.state.wallet.price}</p>
                            <p>EUR: {this.state.wallet.balance}</p>
                        </div>
                    </div>
                    <div className="container">
                        <div className="title">
                            <p>Wallet info</p>
                        </div>
                        <div className="content">
                            <p>ID: {this.props.user.walletId?this.props.user.walletId:654812928341}</p>
                            <p>mail: {this.props.user.email}</p>
                        </div>
                    </div>
                </div>
            )
        }
        else if(isMobileOnly){
            return(
                <div className="walletMobile">
                    <div className="container">
                        <div className="title">
                            <p>My balance</p>
                        </div>
                        <div className="content">
                            <p>SourceCoin: {this.state.wallet.balance*this.state.wallet.price}</p>
                            <p>EUR: {this.state.wallet.balance}</p>
                        </div>
                    </div>
                    <div className="container">
                        <div className="title">
                            <p>Wallet info</p>
                        </div>
                        <div className="content">
                            <p>ID: {this.props.user.walletId}</p>
                            <p>mail: {this.props.user.email}</p>
                        </div>
                    </div>
                </div>
            )
        }
    }

    user = () => {

        if(isBrowser){
            console.log(this.state.information)
            if(this.state.information.length && this.state.information.length > 0){
                var t = new Date(1965,0,1)
                t.setSeconds(this.state.information[0].init_date)
            }
           
            return(
                <div className="wallet">
                    <div className="container">
                        <div className="title">
                            <p>User info</p>
                        </div>
                        <div className="content">
                            <p>Name and Lastname: {this.props.user.name} {this.props.user.lastName}</p>
                            <p>Country: {this.state.user.country}</p>
                            <p>Mail address: {this.props.user.email}</p>
                            <p>Wallet ID: {this.props.user.walletId}</p>
                            <p>Current contract: {this.state.information.length && this.state.information.length > 0?"Yes":"No"}</p>
                        </div>
                        {this.state.information.length && this.state.information.length > 0?<div className="walletMobile">
                        <div className="title">
                            <p>Current contract</p>
                        </div>
                        <div className="content">
                        <div className="information">
                                        
                                        <div>
                                                <p>Proveedor: {this.state.information[0].id_vendedor}</p>
                                                <p>Energia: {this.state.information[0].energy_type}</p>
                                                <p>Fecha inicio: {moment(t).format("DD/MM/YY")}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {this.state.information[0].potencia} kW</p>
                                                <p>Precio: {this.state.information[0].precio} €/kWh</p>
                                                <p>Permanencia: {this.state.information[0].permanecia} días</p>
                                            </div>
                        </div>
                        </div>
                </div>  :null}
                    </div>
                   
                    {/* {this.props.location.state.contractInfo>=0?<div className="wallet">
                    <div className="container">
                        <div className="title">
                            <p>Current contract</p>
                        </div>
                        <div className="content">
                        <div className="information">
                                            <div>
                                                <p>Proveedor: {this.state.contracts[this.props.location.state.contractInfo].provider}</p>
                                                <p>Energia: {this.state.contracts[this.props.location.state.contractInfo].tipo_energia}</p>
                                                <p>Fecha inicio: {this.state.contracts[this.props.location.state.contractInfo].fecha_inicio}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {this.state.contracts[this.props.location.state.contractInfo].potencia_subministrada} kW</p>
                                                <p>Precio: {this.state.contracts[this.props.location.state.contractInfo].precio} €/kWh</p>
                                                <p>Fecha fin: {this.state.contracts[this.props.location.state.contractInfo].fecha_fin}</p>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>  :null} */}
                </div>
            )
        }
        else if(isMobileOnly){
            console.log(this.state.information)
            if(this.state.information.length && this.state.information.length > 0){
                var t = new Date(1965,0,1)
                t.setSeconds(this.state.information[0].init_date)
            }
            return(
                <div className="walletMobile">
                    <div className="container">
                        <div className="title">
                            <p>User info</p>
                        </div>
                        <div className="content">
                            <p>Name and Lastname: {this.props.user.name} {this.props.user.lastName}</p>
                            <p>Country: {this.state.user.country}</p>
                            <p>Mail address: {this.props.user.email}</p>
                            <p>Wallet ID: {this.props.user.walletId}</p>
                            <p>Current contract: {this.state.information.length && this.state.information.length > 0?"Yes":"No"}</p>
                        </div>
                        {this.state.information.length && this.state.information.length > 0?<div className="walletMobile">
                        <div className="title">
                            <p>Current contract</p>
                        </div>
                        <div className="content">
                        <div className="information">
                                        
                        <div>
                                                <p>Proveedor: {this.state.information[0].id_vendedor}</p>
                                                <p>Energia: {this.state.information[0].energy_type}</p>
                                                <p>Fecha inicio: {moment(t).format("DD/MM/YY")}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {this.state.information[0].potencia} kW</p>
                                                <p>Precio: {this.state.information[0].precio} €/kWh</p>
                                                <p>Permanencia: {this.state.information[0].permanecia} días</p>
                                            </div>
                        </div>
                        </div>
                </div>  :null}
            </div> 
                </div>
            )
        }
       
    }
    
    render() {

        

        // if (this.state.redirectCheckout) {
        //     return <Redirect to='/checkout/shipping'/>;
        // }
        if (this.state.contract) {
            return <Redirect to={{
                pathname: '/contract',
                state: {contractInfo: this.state.contractSelected}
            }} />;
        }
        if(isBrowser){
            return(
                <div className="user">
                <Header logged={true}/>
                <div className="userHeader" >
                    <div className="navBarWrapper" >
                        <div className="navBar">
                            <div className="link__separator">
                                <p>|</p>
                            </div>
                            <div className="container" onClick={() => this.selectedOption("home")}>
                                <p className={this.state.options.selected === "home"?"bold":"simple" }>Home</p>
                            </div>
                            <div className="link__separator">
                                <p>|</p>
                            </div>
                            <div className="container" onClick={() => this.selectedOption("wallet")}>
                                <p className={this.state.options.selected === "wallet"?"bold":"simple" }>Wallet</p>
                            </div>
                            <div className="link__separator">
                                <p>|</p>
                            </div>
                            <div className="container" onClick={() => this.selectedOption("information")}>
                                <p className={this.state.options.selected === "information"?"bold":"simple" }>Information</p>
                            </div>
                            <div className="link__separator">
                                <p>|</p>
                            </div>                
                        </div>
                    </div>
                {this.state.options.selected === "home" ? <div className="userBody">
                    {this.marketplace()}
                </div>:null}   
                {this.state.options.selected === "wallet" ? 
                <div className="userBody">
                    {this.wallet()}
                </div>:null}  
                {this.state.options.selected === "information" ? <div className="userBody">
                    {this.user()}
                </div>:null}    
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                <Footer/>
            </div>         
            )
        }
        if(isMobileOnly) {
            return (
                <div className="userMobile">
                <HeaderMobile logged={true}/>
                <div className="userHeader" >
                    <div className="navBarWrapper" >
                        <div className="navBar">
                            <div className="link__separator">
                                <p className="pSeparator">|</p>
                            </div>
                            <div className="container" onClick={() => this.selectedOption("home")}>
                                <p className={this.state.options.selected === "home"?"bold":"simple" }>Home</p>
                            </div>
                            <div className="link__separator">
                                <p className="pSeparator">|</p>
                            </div>
                            <div className="container" onClick={() => this.selectedOption("wallet")}>
                                <p className={this.state.options.selected === "wallet"?"bold":"simple" }>Wallet</p>
                            </div>
                            <div className="link__separator">
                                <p className="pSeparator">|</p>
                            </div>
                            <div className="container" onClick={() => this.selectedOption("information")}>
                                <p className={this.state.options.selected === "information"?"bold":"simple" }>Information</p>
                            </div>
                            <div className="link__separator">
                                <p className="pSeparator">|</p>
                            </div>                
                        </div>
                    </div>
                {this.state.options.selected === "home" ? <div className="userBody">
                    {this.marketplace()}
                </div>:null}   
                {this.state.options.selected === "wallet" ? 
                <div className="userBody">
                    {this.wallet()}
                </div>:null}  
                {this.state.options.selected === "information" ? <div className="userBody">
                    {this.user()}
                </div>:null}   
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <FooterMobile/>
            </div>         
            )
        }
    }
}

const mapStateToProps = (state)=>{
    // console.log(state.authReducer.serverState)

    return {
        token: state.authReducer.serverState.TOKEN_LOGIN,
        logged: state.authReducer.serverState.logged,
        contracts:state.authReducer.serverState.contracts,
        user: state.authReducer.serverState.userInfo
        // items: state.cartReducer.addedItems,
        // category: state.storeReducer.category
    }
}

const mapDispatchToProps= (dispatch)=>{
    return{
        getContracts: (trips) => dispatch(getContracts(contracts)),
        // deleteFromCart: (id)=>{dispatch(deleteFromCart(id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(User);