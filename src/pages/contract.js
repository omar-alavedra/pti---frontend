import React from 'react'
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Redirect } from 'react-router-dom';

//-- COMPONENTS -- desktop
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';

//--3rd party components
import { isBrowser, isMobileOnly} from "react-device-detect";
const axios = require('axios');
import moment from "moment";
//Redux functions
// import {deleteFromCart } from '../store/actions/index';


class Contract extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            contract: false,
            ownContract: [],
            options: {
                selected: 'home'
            },
            user: {
                name: "John",
                lastName: "Appleseed",
                country: "Spain",
                walletID: 123456789,
                mail: "example@mail.com",
                contract: false
            },
            wallet: {
                balance: 0,
                price: 0,
                id: 123456789,
            },
            contracts: [
                {
                    provider: "Tanner",
                    tipo_energia: "eolica",
                    fecha_inicio: "1/12/2019",
                    fecha_fin: "31/12/2019",
                    potencia_subministrada: "600",
                    precio: "0.025"
                },
                {
                    provider: "Melissa",
                    tipo_energia: "solar",
                    fecha_inicio: "1/12/2019",
                    fecha_fin: "31/12/2019",
                    potencia_subministrada: "500",
                    precio: "0.035"
                },
                {
                    provider: "Joan",
                    tipo_energia: "solar",
                    fecha_inicio: "1/12/2019",
                    fecha_fin: "31/12/2019",
                    potencia_subministrada: "400",
                    precio: "0.025"
                },
            ]
        };
        this.contract = this.contract.bind(this);
        this.selectedOption = this.selectedOption.bind(this);
    }
    contract = (id) => {
        console.log(id)
        let data = JSON.stringify({
            contractid: id,
        })
        let write = false;
        let information=[]
        axios.post('https://cors-anywhere.herokuapp.com/http://34.70.95.68:3000/api/contract/acceptContract',data, {headers: {'auth-token':this.props.token, 'Content-Type': 'application/json', "Accept": "application/json", } } )
        .then(function (response) {
          console.log(response);
          // this.props.getContracts(response)
        //   dispatch(saveContracts(response.data));
        // this.setState({
        //     contract: true,
        // })
    })
        .catch(function (error) {
          console.log(error);
        });
        axios.post('https://cors-anywhere.herokuapp.com/http://34.70.95.68:3000/api/contract/listOwnContract',{'auth-token':this.props.token}, {headers: {'auth-token':this.props.token, 'Content-Type': 'application/json', "Accept": "application/json", } } )
        .then(function (response) {
          console.log(response);
          // this.props.getContracts(response)
        //   dispatch(saveContracts(response.data));
        write = true
        information=response.data
        
    })
        .catch(function (error) {
          console.log(error);
        });
            console.log(information)
            this.setState({
                ownContract: information,
                contract: true,
            })

    }

    selectedOption = (e) => {
        this.setState({
            ...this.state,
            options: {
                ...this.state.options,
                selected: e
            }
        })
    }
    componentWillMount(){
        console.log(this.props)
    }
    marketplace = () => {
        if(isBrowser){
            return(
                <div className="marketPlace">
                    <div className="container">
                        <div className="title">
                            <p>Marketplace</p>
                        </div>
                        <div className="content">
                            {this.state.contracts.map((contract,index) => {
                                return(
                                    <div className="contractCard">
                                        <div className="information">
                                            <div>
                                                <p>Proveedor: {contract.provider}</p>
                                                <p>Energia: {contract.tipo_energia}</p>
                                                <p>Fecha inicio: {contract.fecha_inicio}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {contract.potencia_subministrada} kW</p>
                                                <p>Precio: {contract.precio} €/kWh</p>
                                                <p>Fecha fin: {contract.fecha_fin}</p>
                                            </div>
                                        </div>
                                        <button className="buttonContainer" onClick={()=>this.contract(contract._id)}>CONTRATAR</button>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            )
        }
        else if(isMobileOnly) {
        return(
            <div className="marketPlaceMobile">
                <div className="container">
                    <div className="title">
                        <p>Marketplace</p>
                    </div>
                    <div className="content">
                        {this.state.contracts.map((contract,index) => {
                            return(
                                <div className="contractCard">
                                    <div className="information">
                                        <div>
                                            <p>Proveedor: {contract.provider}</p>
                                            <p>Energia: {contract.tipo_energia}</p>
                                            <p>Fecha inicio: {contract.fecha_inicio}</p>
                                        </div>
                                        <div>
                                            <p>Potencia: {contract.potencia_subministrada} kW</p>
                                            <p>Precio: {contract.precio} €/kWh</p>
                                            <p>Fecha fin: {contract.fecha_fin}</p>
                                        </div>
                                    </div>
                                    <button className="buttonContainer">Contratar</button>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
                    }
    }

    wallet = () => {
        if(isBrowser){
            return(
                <div className="wallet">
                    <div className="container">
                        <div className="title">
                            <p>My balance</p>
                        </div>
                        <div className="content">
                            <p>ETH: {this.state.wallet.balance*this.state.wallet.price}</p>
                            <p>EUR: {this.state.wallet.balance}</p>
                            <p>Current ETH price: {this.state.wallet.price}</p>
                        </div>
                    </div>
                    <div className="container">
                        <div className="title">
                            <p>Wallet info</p>
                        </div>
                        <div className="content">
                            <p>ID: {this.state.wallet.id}</p>
                            <p>mail: {this.state.user.mail}</p>
                        </div>
                    </div>
                </div>
            )
        }
        else if(isMobileOnly){
            return(
                <div className="walletMobile">
                    <div className="container">
                        <div className="title">
                            <p>My balance</p>
                        </div>
                        <div className="content">
                            <p>ETH: {this.state.wallet.balance*this.state.wallet.price}</p>
                            <p>EUR: {this.state.wallet.balance}</p>
                            <p>Current ETH price: {this.state.wallet.price}</p>
                        </div>
                    </div>
                    <div className="container">
                        <div className="title">
                            <p>Wallet info</p>
                        </div>
                        <div className="content">
                            <p>ID: {this.state.wallet.id}</p>
                            <p>mail: {this.state.user.mail}</p>
                        </div>
                    </div>
                </div>
            )
        }
    }

    user = () => {
        if(isBrowser){
            return(
                <div className="wallet">
                    <div className="container">
                        <div className="title">
                            <p>User info</p>
                        </div>
                        <div className="content">
                            <p>Name and Lastname: {this.state.user.name} {this.state.user.lastName}</p>
                            <p>Country: {this.state.user.country}</p>
                            <p>Mail address: {this.state.user.mail}</p>
                            <p>Wallet ID: {this.state.user.walletID}</p>
                            <p>Current contract: {this.state.user.contract?"Yes":"No"}</p>
                        </div>
                    </div>
                </div>
            )
        }
        else if(isMobileOnly){
            return(
                <div className="walletMobile">
                    <div className="container">
                        <div className="title">
                            <p>User info</p>
                        </div>
                        <div className="content">
                            <p>Name and Lastname: {this.state.user.name} {this.state.user.lastName}</p>
                            <p>Country: {this.state.user.country}</p>
                            <p>Mail address: {this.state.user.mail}</p>
                            <p>Wallet ID: {this.state.user.walletID}</p>
                            <p>Current contract: {this.state.user.contract?"Yes":"No"}</p>
                        </div>
                    </div>
                </div>
            )
        }
       
    }
    
    render() {

        // let itemList = this.props.items.map((item,index)=>{

        //     return(
        //         <div className="itemCard" key={index}>
        //                 <div>
        //                     {item.img}
        //                 </div>
        //                 <div className="card-image">
        //                     <span className="card-title">{item.title}</span>
        //                     <button onClick={()=>this.deleteItem(item.id)}>delete</button>
        //                 </div>
        //                 <div className="card-content">
        //                     <p>{item.desc}</p>
        //                     <p><b>Price: {item.price}$</b></p>
        //                 </div>
        //          </div>
        //     )
        // })

        if (this.state.back) {
            return <Redirect to={{
                pathname: '/user',
                state: {contractInfo: this.props.location.state.contractInfo}
            }}/>;
        }
        if (this.state.contract) {
            return <Redirect to={{
                pathname: '/user',
                state: {ownContract: this.props.location.state.contractInfo}
            }}/>;
        }
        if(isBrowser){
            console.log(this.props.location.state.contractInfo)
            var t = new Date(1965,0,1)
            t.setSeconds(this.props.location.state.contractInfo.creation_date)
            return(
                
                <div className="contract">
                <Header logged={true}/>
                <div className="userBody">
                <div className="wallet">
                    <div className="container">
                        <div className="title">
                            <p>Contract</p>
                        </div>
                        <div className="content">
                        <div className="information">
                                            <div>
                                                <p>Proveedor: {this.props.location.state.contractInfo.id_vendedor}</p>
                                                <p>Energia: {this.props.location.state.contractInfo.energy_type}</p>
                                                <p>Fecha inicio: {moment(t).format("DD/MM/YY")}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {this.props.location.state.contractInfo.potencia} kW</p>
                                                <p>Precio: {this.props.location.state.contractInfo.precio} €/kWh</p>
                                                <p>Permanencia: {this.props.location.state.contractInfo.permanecia} días</p>
                                            </div>
                                        </div>
                        </div>
                        <button className="buttonContainer" onClick={()=>this.contract(this.props.location.state.contractInfo._id)}>Aceptar</button>
                    </div>
                    
                </div>  
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                <Footer/>
            </div>         
            )
        }
        if(isMobileOnly) {
            var t = new Date(1965,0,1)
            t.setSeconds(this.props.location.state.contractInfo.creation_date)
            return (
                <div className="contractMobile">
                <HeaderMobile logged={true}/>
                <div className="userBody">
                <div className="walletMobile">
                    <div className="container">
                        <div className="title">
                            <p>Contract</p>
                        </div>
                        <div className="content">
                        <div className="information">
                        <div>
                                                <p>Proveedor: {this.props.location.state.contractInfo.id_vendedor}</p>
                                                <p>Energia: {this.props.location.state.contractInfo.energy_type}</p>
                                                <p>Fecha inicio: {moment(t).format("DD/MM/YY")}</p>
                                            </div>
                                            <div>
                                                <p>Potencia: {this.props.location.state.contractInfo.potencia} kW</p>
                                                <p>Precio: {this.props.location.state.contractInfo.precio} €/kWh</p>
                                                <p>Permanencia: {this.props.location.state.contractInfo.permanecia} días</p>
                                            </div>
                                        </div>
                        </div>
                        <button className="buttonContainer" onClick={()=>this.contract(this.props.location.state.contractInfo._id)}>Aceptar</button>

                    </div>
                </div>     
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                <FooterMobile/>
            </div>          
            )
        }
    }
}

const mapStateToProps = (state)=>{
    console.log(state)
    return {
        token: state.authReducer.serverState.TOKEN_LOGIN,
        // items: state.cartReducer.addedItems,
        // category: state.storeReducer.category
    }
}

const mapDispatchToProps= (dispatch)=>{
    return{
        // deleteFromCart: (id)=>{dispatch(deleteFromCart(id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Contract);